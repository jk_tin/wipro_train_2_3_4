package com.wipro.angular_sprinboot.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/hello")
	String sayHello()
	{
		
		return "Hello";
	}
}
