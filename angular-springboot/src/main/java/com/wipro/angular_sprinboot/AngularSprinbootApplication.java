package com.wipro.angular_sprinboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularSprinbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularSprinbootApplication.class, args);
	}

}
