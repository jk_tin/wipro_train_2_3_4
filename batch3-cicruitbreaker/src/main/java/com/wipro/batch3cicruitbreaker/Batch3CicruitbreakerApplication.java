package com.wipro.batch3cicruitbreaker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3CicruitbreakerApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3CicruitbreakerApplication.class, args);
	}

}
