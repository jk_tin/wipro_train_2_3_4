package com.wipro.batch3cicruitbreaker.dto;

public class Payment {
	int id;

	double amount;

	double orderId;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getOrderId() {
		return orderId;
	}

	public void setOrderId(double orderId) {
		this.orderId = orderId;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", amount=" + amount + ", orderId=" + orderId + "]";
	}

}
