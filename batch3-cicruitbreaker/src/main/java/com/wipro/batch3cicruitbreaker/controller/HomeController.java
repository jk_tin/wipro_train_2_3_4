package com.wipro.batch3cicruitbreaker.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wipro.batch3cicruitbreaker.dto.Payment;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

@RestController
public class HomeController {

	@Autowired
	RestTemplate restTemplate;
	
	@CircuitBreaker(name = "paymentCircuitBreaker", fallbackMethod = "showServiceDown")
	@GetMapping("/payment/{id}")
	String getPayment(@PathVariable int id )
	{		
		String url="http://localhost:9091/payment/"+id;
		Payment payment =restTemplate.getForObject(url, Payment.class);
		return payment.toString();
		 
	}
	
	String showServiceDown(Throwable throwable)
	{
		
		return "System is down";
	}
}
