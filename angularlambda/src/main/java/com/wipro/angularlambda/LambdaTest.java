package com.wipro.angularlambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.services.lambda.runtime.RequestHandler;
public class LambdaTest {
	
	public String handleRequest(String input, Context context) {
        context.getLogger().log("Input: " + input);
        return "Hello World - " + input;
    }
	public String handleRequest2(String input, Context context) {
        context.getLogger().log("Input2: " + input);
        return "Hello World2 - " + input;
    }

}
