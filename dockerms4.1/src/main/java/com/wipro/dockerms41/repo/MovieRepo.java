package com.wipro.dockerms41.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.dockerms41.entity.Movie;

@Repository
public interface MovieRepo extends JpaRepository<Movie, Integer> {

}
