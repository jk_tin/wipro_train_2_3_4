package com.wipro.dockerms41.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.dockerms41.entity.Movie;
import com.wipro.dockerms41.service.MovieService;

@RestController
public class MovieController {
	@Autowired
	MovieService movieService;
	@GetMapping("/movie")
	List<Movie> findAll()
	{
		return movieService.findAll();
		
	}

}
