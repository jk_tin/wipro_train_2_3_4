package com.wipro.dockerms41.service;

import java.util.List;

import com.wipro.dockerms41.entity.Movie;

public interface MovieService {

	List<Movie> findAll();
	
}
