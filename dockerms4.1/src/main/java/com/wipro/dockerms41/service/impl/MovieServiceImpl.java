package com.wipro.dockerms41.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.dockerms41.entity.Movie;
import com.wipro.dockerms41.repo.MovieRepo;
import com.wipro.dockerms41.service.MovieService;

@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieRepo moveRepo;
	
	@Override
	public List<Movie> findAll() {
		// TODO Auto-generated method stub
		return moveRepo.findAll();
	}

}
