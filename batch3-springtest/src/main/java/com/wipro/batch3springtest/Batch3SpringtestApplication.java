package com.wipro.batch3springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3SpringtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3SpringtestApplication.class, args);
	}

}
