package com.wipro.batch3springtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3springtest.service.GreetService;

@RestController
public class HomeController {
	
	@Autowired
	GreetService greetService;
	
	@GetMapping("/greet")
	String greet()
	{
		return "Hello World!!!";
	}
	
	@GetMapping("/welcome")
	String welcome()
	{
		return  greetService.welcomeToWipro();
	}
	
}
