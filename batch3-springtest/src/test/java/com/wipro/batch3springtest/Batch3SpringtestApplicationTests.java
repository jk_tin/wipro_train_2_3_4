package com.wipro.batch3springtest;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Batch3SpringtestApplicationTests {

	@Test
	void contextLoads() {
		
		System.out.println("--Context Loaded--");
	}

}
