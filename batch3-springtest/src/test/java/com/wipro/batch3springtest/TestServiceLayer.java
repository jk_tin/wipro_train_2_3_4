package com.wipro.batch3springtest;
import com.wipro.batch3springtest.controller.*;
import com.wipro.batch3springtest.service.GreetService;

import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.containsString;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(HomeController.class)
class TestServiceLayer {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private GreetService service;

	@Test
	void greetingShouldReturnMessageFromService() throws Exception {
		when(service.welcomeToWipro()).thenReturn("Welcome To Wipro!");
		
		this.mockMvc
		   .perform(get("/welcome"))
		   .andDo(print()).andExpect(status().isOk())
				.andExpect(content()
						.string(containsString("Welcome To Wipro")));
	}

}
