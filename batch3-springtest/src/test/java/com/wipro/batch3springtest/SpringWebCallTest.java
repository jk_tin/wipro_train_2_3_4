package com.wipro.batch3springtest;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class SpringWebCallTest {
	
	  @Autowired 	
	  TestRestTemplate restTeplate;
	  
	  @LocalServerPort
	  int port;
		
	    @Test
		void contextLoads()
		{
			String url="http://localhost:"+port+"/greet";
			String str=restTeplate.getForObject(url, String.class);
			assertTrue(str.equalsIgnoreCase("Hello World!!"));
			
		}
			

}
