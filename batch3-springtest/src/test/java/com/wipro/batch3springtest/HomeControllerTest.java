package com.wipro.batch3springtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.wipro.batch3springtest.controller.HomeController;

@SpringBootTest
class HomeControllerTest {

	@Autowired 
	HomeController homeConlroller;
	
	@Test
	void contextLoads() {
		
		assertThat(homeConlroller).isNotNull();
		
		
	}

}
