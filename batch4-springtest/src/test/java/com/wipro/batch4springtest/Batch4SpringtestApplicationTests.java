package com.wipro.batch4springtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.wipro.batch4springtest.controller.HomeController;

@SpringBootTest
class Batch4SpringtestApplicationTests {

	@Autowired
	HomeController homeController;
	
	@Test
	void contextLoads() {
		 assertThat(homeController).isNotNull();
	}

}
