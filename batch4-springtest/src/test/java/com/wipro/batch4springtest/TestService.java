package com.wipro.batch4springtest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.wipro.batch4springtest.controller.*;
import com.wipro.batch4springtest.service.HelloService;
@WebMvcTest(HelloController.class)
class TestService {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	HelloService helloService;
	
	@Test
	void test() {
		when(helloService.sayHello())
		.thenReturn("Hello@");
		
		try {
			this.mockMvc
			.perform(get("/hello"))
			.andDo(print())
			.andExpect(status().isOk())
			.andExpect(content()
			.string(containsString("Hello@")));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
