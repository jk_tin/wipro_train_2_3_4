package com.wipro.batch4springtest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class GreetApiTest {

	@Autowired
	TestRestTemplate testRestTemplate;
	
	@LocalServerPort
	int port;
	
	@Test
	void test() {
		 
		
		String url="http://localhost:"+port+"/greet";
		assertThat(testRestTemplate
		.getForObject(url, String.class))
		.contains("Hello Wor");
		
		
	}

}
