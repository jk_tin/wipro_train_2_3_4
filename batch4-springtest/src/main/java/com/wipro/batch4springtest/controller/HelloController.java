package com.wipro.batch4springtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4springtest.service.HelloService;

@RestController
public class HelloController {

	@Autowired
	HelloService helloService;
	
	@GetMapping("/hello")
	String hello()
	{
		return helloService.sayHello();
		
	}
	
	
}
