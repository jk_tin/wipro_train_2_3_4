package com.wipro.batch4springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4SpringtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4SpringtestApplication.class, args);
	}

}
