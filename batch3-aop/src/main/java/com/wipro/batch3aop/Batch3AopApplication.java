package com.wipro.batch3aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class Batch3AopApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3AopApplication.class, args);
	}

}
