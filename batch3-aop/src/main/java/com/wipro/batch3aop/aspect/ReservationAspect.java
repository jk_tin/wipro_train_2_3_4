package com.wipro.batch3aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class ReservationAspect {
	
	@Before("execution(* com.wipro.batch3aop.controller.ReservationController..checkIn(..))")
	void checkVacCard()
	{
		System.out.println("--Vaccinaton Card Checked- ");
	}
	
	@After("execution(* com.wipro.batch3aop.controller.ReservationController..checkOut(..))")
	void sayThanks()
	{
		System.out.println("--Thank you for staying with us- ");
	}
	
	@Around("execution(* com.wipro.batch3aop.controller.ReservationController..orderFood(..))")
	void callAround(ProceedingJoinPoint pjp)
	{
		
		System.out.println("--Before ordering food- ");
		try {
			pjp.proceed();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("--After ordering food- ");
		
	}
	
	

}
