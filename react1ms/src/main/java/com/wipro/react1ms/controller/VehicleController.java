package com.wipro.react1ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.react1ms.entity.Vehicle;
import com.wipro.react1ms.service.VehicleService;

@RestController
public class VehicleController {

	@Autowired
	VehicleService vehicleService;
	
	@GetMapping("/vehicle")
	List<Vehicle> findAll()
	{
		return vehicleService.findAll();
	}
}
