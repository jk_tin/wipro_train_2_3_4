package com.wipro.react1ms.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.react1ms.entity.Vehicle;

public interface VehicleRepo extends JpaRepository<Vehicle, Integer> {

}
