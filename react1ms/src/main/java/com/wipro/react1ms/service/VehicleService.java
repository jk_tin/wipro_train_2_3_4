package com.wipro.react1ms.service;
import com.wipro.react1ms.entity.Vehicle;
import java.util.*;
public interface VehicleService {
	
	List<Vehicle> findAll();
	
}
