package com.wipro.react1ms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.react1ms.entity.Vehicle;
import com.wipro.react1ms.repo.VehicleRepo;
import com.wipro.react1ms.service.VehicleService;
@Service
public class VehicleImpl implements VehicleService {

	@Autowired
	VehicleRepo vehicleRepo;
	
	@Override
	public List<Vehicle> findAll() {
		 
		return vehicleRepo.findAll();
	}

}
