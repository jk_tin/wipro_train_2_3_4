package com.wipro.react1ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class React1msApplication {

	public static void main(String[] args) {
		SpringApplication.run(React1msApplication.class, args);
	}

}
