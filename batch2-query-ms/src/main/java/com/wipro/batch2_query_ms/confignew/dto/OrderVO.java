package com.wipro.batch2_query_ms.confignew.dto;

import java.time.LocalDateTime;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

public class OrderVO {
 		int id;
		String orderDesc;
			 
		String orderId;
				 
		String status;
		
		LocalDateTime orderDateTime;

		public String getOrderDesc() {
			return orderDesc;
		}

		public void setOrderDesc(String orderDesc) {
			this.orderDesc = orderDesc;
		}

		public String getOrderId() {
			return orderId;
		}

		public void setOrderId(String orderId) {
			this.orderId = orderId;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public LocalDateTime getOrderDateTime() {
			return orderDateTime;
		}

		public void setOrderDateTime(LocalDateTime orderDateTime) {
			this.orderDateTime = orderDateTime;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		@Override
		public String toString() {
			return "OrderVO [id=" + id + ", orderDesc=" + orderDesc + ", orderId=" + orderId + ", status=" + status
					+ ", orderDateTime=" + orderDateTime + "]";
		}

		 
		
}
