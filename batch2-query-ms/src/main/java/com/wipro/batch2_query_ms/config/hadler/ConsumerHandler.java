package com.wipro.batch2_query_ms.config.hadler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
 

import com.wipro.batch2_query_ms.confignew.dto.OrderVO;
import com.wipro.batch2_query_ms.entity.Order;
import com.wipro.batch2_query_ms.repo.OrderRepo;
 
 
 

 
@Component
public class ConsumerHandler {
	
	@Autowired
    OrderRepo orderRepo;
	
	@KafkaListener(topics = "order-topic", groupId = "batch3-group-id",containerFactory = "kafkaListenerContainerFactory"  ,properties = {"spring.json.value.default.type=com.wipro.batch2_query_ms.entity.Order"})
    public void listen(Order order) {
        System.out.println("Received message: " + order);
        Order orderData=orderRepo.findByOrderId(order.getOrderId());
        if(null==orderData)
        {
        	orderData= new Order();
        	 
        }	       
    	
    	orderData.setOrderDesc(order.getOrderDesc());
    	orderData.setOrderId(order.getOrderId());
    	orderData.setStatus(order.getStatus());
        orderRepo.save(orderData);
        
        
    }
//    @KafkaListener(topics = "order-topic", groupId = "batch2-group-id",containerFactory = "kafkaListenerContainerFactory")
//    public void listen(OrderVO order) {
//    	System.out.println("Received message: " + order);
//    	Order orderD= new Order();
//    	orderD.setOrderDesc(order.getOrderDesc());
//    	orderD.setOrderId(order.getOrderId());
//    	orderD.setStatus(order.getStatus());
//    	
//        System.out.println("Received message: " + order);
//        
//        Order orderData=orderRepo.findByOrderId(order.getOrderId());
//        if(null==orderData)
//        {
//        	orderRepo.save(orderD);
//        }
//        else        
//        {
//        	orderD.setId(orderData.getId());
//        	
//        }
        
  //  }
}
