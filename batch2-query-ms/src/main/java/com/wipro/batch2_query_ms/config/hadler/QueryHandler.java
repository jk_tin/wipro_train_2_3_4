package com.wipro.batch2_query_ms.config.hadler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch2_query_ms.entity.Order;
import com.wipro.batch2_query_ms.repo.OrderRepo;

@Service
public class QueryHandler {
	
	@Autowired
	OrderRepo orderRepo;
	
	public List<Order> findAll(){
		
		return orderRepo.findAll();
	}
	
    public Order findById(int id){
		
		return orderRepo.findById(id).get();
	}
	
	

}
