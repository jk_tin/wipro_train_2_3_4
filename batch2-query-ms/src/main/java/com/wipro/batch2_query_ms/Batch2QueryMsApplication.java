package com.wipro.batch2_query_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2QueryMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2QueryMsApplication.class, args);
	}

}
