package com.wipro.batch2_query_ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch2_query_ms.config.hadler.ConsumerHandler;
import com.wipro.batch2_query_ms.config.hadler.QueryHandler;
import com.wipro.batch2_query_ms.entity.Order;

@RestController
public class OrderController {
	
	@Autowired
	QueryHandler queryHandler;
	
	@GetMapping("/order")
	List<Order> findAll()
	{
		return queryHandler.findAll();
		
	}
	@GetMapping("/order/{id}")
	List<Order> findAll(@PathVariable int id)
	{
		return queryHandler.findAll();
		
	}

}
