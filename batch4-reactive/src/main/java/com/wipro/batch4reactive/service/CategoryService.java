package com.wipro.batch4reactive.service;

import com.wipro.batch4reactive.entity.Category;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CategoryService {
	
	
	Flux<Category> findAll();
	Mono<Category>findById(int id);
	Mono<Category> save(Category category);
	Mono<Void> deleteById(int id);
	 

}
