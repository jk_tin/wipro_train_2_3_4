package com.wipro.batch4reactive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch4reactive.entity.Category;
import com.wipro.batch4reactive.repo.CategoryRepo;
import com.wipro.batch4reactive.service.CategoryService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CategoryServiceImpl implements CategoryService {

	@Autowired
	CategoryRepo categoryRepo;
	@Override
	public Flux<Category> findAll() {
		// TODO Auto-generated method stub
		return categoryRepo.findAll();
	}

	@Override
	public Mono<Category> findById(int id) {
		// TODO Auto-generated method stub
		return categoryRepo.findById(id);
	}

	@Override
	public Mono<Category> save(Category category) {
		// TODO Auto-generated method stub
		return categoryRepo.save(category);
	}

	@Override
	public Mono<Void> deleteById(int id) {
		// TODO Auto-generated method stub
		return categoryRepo.deleteById(id);
	}

}
