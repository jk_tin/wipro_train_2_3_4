package com.wipro.batch4reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4ReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4ReactiveApplication.class, args);
	}

}
