package com.wipro.batch4reactive.controller;

import java.time.Duration;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class HomeController {
	
	//Mono -> 0..1 objects
	
	@GetMapping("/mono")
	Mono<String> getMono()
	{
		return Mono
		.just("Hello from Mono")
		.log();
		
	}
	@GetMapping("/flux")
	Flux<String> getFlux()
	{
		String arr[]= {"Jayanta","Moumita","Vasu","Shiva","Sai"};
		return Flux
		.fromArray(arr)
		.take(2)
	    .delayElements(Duration.ofSeconds(1))
	    .repeat()
		.log();
		
	}
	
	

}
