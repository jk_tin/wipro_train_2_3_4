package com.wipro.batch4reactive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4reactive.service.CategoryService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import com.wipro.batch4reactive.entity.*;
@RestController
public class CategoryController {
	
	@Autowired
	CategoryService categoryService;
	
	@GetMapping("/cat")
	Flux<Category> findAll()
	{
		return categoryService.findAll();
		
	}
	@GetMapping("/cat/{id}")
	Mono<Category> findById(@PathVariable int id)
	{
		return categoryService.findById(id);
		
	}
	@PostMapping("/cat")
	Mono<Category> save(@RequestBody Category category)
	{
		return categoryService.save(category);
		
	}
	
	@DeleteMapping("/cat/{id}")
	Mono<Void> deleteById(@PathVariable int id)
	{
		return categoryService.deleteById(id);
		
	}
	@PutMapping("/cat")
	Mono<Category> update(@RequestBody Category category)
	{
		return categoryService.save(category);
		
	}

}
