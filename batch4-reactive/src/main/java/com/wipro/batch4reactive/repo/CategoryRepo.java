package com.wipro.batch4reactive.repo;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4reactive.entity.Category;

@Repository
public interface CategoryRepo extends R2dbcRepository<Category, Integer> {

}
