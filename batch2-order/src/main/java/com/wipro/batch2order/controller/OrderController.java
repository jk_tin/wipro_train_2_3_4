package com.wipro.batch2order.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch2order.dto.OrderVO;
import com.wipro.batch2order.entity.*;
import com.wipro.batch2order.service.OrderService;
 
import com.wipro.batch2order.dto.Payment;
@RestController
public class OrderController {
	
	@Autowired
	OrderService orderService;
//	@Autowired
//	PaymentServiceIntegration paymentServiceIntegration;
	
	
	@PostMapping("/order")
	void save(@RequestBody OrderVO orderVO)
	{
		
		orderService.save(orderVO);
		
	}
	
	@GetMapping("/order")
	OrderVO find()
	{	
		OrderVO orderVO= new OrderVO();
		
		Order order= new Order();
		order.setId(1);
		order.setDescription("Order");
		order.setOrderStatus("P");
		
		Payment payment= new Payment();
		payment.setAmount(100d);
		payment.setId(1);
		payment.setPaid(false);
		
		orderVO.setOrder(order);
		orderVO.setPayment(payment);
		return orderVO;
		
		
 
	 
		
	}

//	@GetMapping("/payments")
//	List<Payment> findAllPaymets()
//	{
//		return paymentServiceIntegration.findAll();
//		
//	}
//    
}
