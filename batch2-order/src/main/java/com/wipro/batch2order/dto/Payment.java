package com.wipro.batch2order.dto;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
 
public class Payment {
	
	int id;
	
	 
	double amount;
	
	 
	double ordrId;
	
	 
	boolean isPaid;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getOrdrId() {
		return ordrId;
	}

	public void setOrdrId(double ordrId) {
		this.ordrId = ordrId;
	}

	public boolean isPaid() {
		return isPaid;
	}

	public void setPaid(boolean isPaid) {
		this.isPaid = isPaid;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", amount=" + amount + ", ordrId=" + ordrId + ", isPaid=" + isPaid + "]";
	}
	
	
	

}
