package com.wipro.batch2order.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.wipro.batch2order.dto.OrderVO;
import com.wipro.batch2order.dto.Payment;
import com.wipro.batch2order.entity.Order;
import com.wipro.batch2order.repo.OrderRepo;
import com.wipro.batch2order.service.OrderService;
import org.springframework.web.client.RestTemplate; 
 
@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	OrderRepo orderRepo;
	
	@Autowired
	RestTemplate restTemplate;
	
	@Override
	public void save(OrderVO orderVO) {
	 
		Order order=orderRepo.save(orderVO.getOrder());
		
		String url="http://PAYMENT-MS/payment";
		Payment pmnt= orderVO.getPayment();
		pmnt.setOrdrId(order.getId());
		//RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Payment> response=restTemplate.postForEntity(url, pmnt, Payment.class);
		Payment pmt=response.getBody();
		System.out.println(pmt);
		if(response.getStatusCode().value()==200)			
		{
			order.setOrderStatus("P");
			orderRepo.save(order);
		}
		
	}

	 
 

}
