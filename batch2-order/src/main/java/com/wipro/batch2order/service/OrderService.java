package com.wipro.batch2order.service;

import com.wipro.batch2order.dto.OrderVO;
import com.wipro.batch2order.entity.Order;

public interface OrderService {

	void save(OrderVO order);
	
}
