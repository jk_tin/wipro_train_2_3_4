package com.wipro.batch2order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
 
 
@SpringBootApplication
@EnableDiscoveryClient
public class Batch2OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2OrderApplication.class, args);
	}

}
