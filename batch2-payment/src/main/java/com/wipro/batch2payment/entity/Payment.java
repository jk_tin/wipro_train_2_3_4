package com.wipro.batch2payment.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="payment")
public class Payment {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="amount")
	double amount;
	
	@Column(name="order_id")
	double ordrId;
	
	 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getOrdrId() {
		return ordrId;
	}

	public void setOrdrId(double ordrId) {
		this.ordrId = ordrId;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", amount=" + amount + ", ordrId=" + ordrId + "]";
	}

	 
 

}
