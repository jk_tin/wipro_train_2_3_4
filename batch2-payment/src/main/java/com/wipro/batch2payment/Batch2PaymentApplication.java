package com.wipro.batch2payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Batch2PaymentApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2PaymentApplication.class, args);
	}

}
