package com.wipro.batch2payment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch2payment.entity.Payment;
import com.wipro.batch2payment.service.PaymentService;

@RestController
public class PaymentController {

	@Autowired
	PaymentService paymentService;
	
	@GetMapping("/payment")
	public List<Payment> findAll() {
		 
		return paymentService.findAll();
	}

	@GetMapping("/payment/{id}")
	public Payment findById(@PathVariable int id) {
		 
		return paymentService.findById(id);
	}

	@PostMapping("/payment")
	public Payment save(@RequestBody Payment payment) {
		return paymentService.save(payment);

	}

	@DeleteMapping("/payment/{id}")
	public void deleteById(@PathVariable int id) {
		paymentService.deleteById(id);

	}
	
	@PutMapping("/payment")
	public void update(@RequestBody Payment payment) {
		paymentService.save(payment);

	}
}
