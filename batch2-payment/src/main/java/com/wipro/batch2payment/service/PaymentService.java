package com.wipro.batch2payment.service;
import com.wipro.batch2payment.entity.*;
import java.util.List;
public interface PaymentService {
	
	List<Payment> findAll();
	Payment findById(int id);
	Payment save(Payment payment);
	void deleteById(int id);

}
