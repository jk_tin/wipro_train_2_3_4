package com.wipro.batch2payment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch2payment.entity.Payment;
import com.wipro.batch2payment.repo.PaymentRepo;
import com.wipro.batch2payment.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentRepo paymentRepo;
	
	
	@Override
	public List<Payment> findAll() {
		 
		return paymentRepo.findAll();
	}

	@Override
	public Payment findById(int id) {
		 
		return paymentRepo.findById(id).get();
	}

	@Override
	public Payment save(Payment payment) {
		return paymentRepo.save(payment);

	}

	@Override
	public void deleteById(int id) {
		paymentRepo.deleteById(id);

	}

}
