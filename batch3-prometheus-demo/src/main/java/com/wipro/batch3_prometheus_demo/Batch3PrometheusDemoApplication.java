package com.wipro.batch3_prometheus_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3PrometheusDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3PrometheusDemoApplication.class, args);
	}

}
