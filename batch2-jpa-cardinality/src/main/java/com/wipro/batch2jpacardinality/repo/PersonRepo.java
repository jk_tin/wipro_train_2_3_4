package com.wipro.batch2jpacardinality.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch2jpacardinality.entity.Person;

@Repository
public interface PersonRepo extends JpaRepository<Person, Integer> {

}
