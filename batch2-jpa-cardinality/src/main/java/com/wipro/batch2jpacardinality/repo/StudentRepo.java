package com.wipro.batch2jpacardinality.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch2jpacardinality.entity.Cart;
import com.wipro.batch2jpacardinality.entity.Person;
import com.wipro.batch2jpacardinality.entity.Student;

@Repository
public interface StudentRepo extends JpaRepository<Student, Integer> {

}
