package com.wipro.batch2jpacardinality.controller;

import java.util.Set;
import java.util.HashSet;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.wipro.batch2jpacardinality.entity.*;
import com.wipro.batch2jpacardinality.repo.CartRepo;
import com.wipro.batch2jpacardinality.repo.PersonRepo;
import com.wipro.batch2jpacardinality.repo.StudentRepo;
 

@RestController
public class HomeController {
	@Autowired
	PersonRepo personRepo;
	
	@Autowired
	CartRepo cartRepo;
	
	@Autowired
	StudentRepo studentRepo;
	
	@GetMapping("/person")
	void savePerson()
	{
		Person p= new Person();
		p.setName("Jayanta");		
		Pan pan = new Pan();
		pan.setPanNumber("AEGTP3971D");		
		p.setPan(pan);
		personRepo.save(p);	
		
	}
	
	@GetMapping("/cart")
	void saveCart()
	{
		Cart cart= new Cart();
		Item item1 =new Item();
		item1.setDescription("item-01");
		item1.setCart(cart);
		
		Item item2 =new Item();
		item2.setDescription("item-02");
		item2.setCart(cart);
		
		Item item3 =new Item();
		item3.setDescription("item-03");
		item3.setCart(cart);
	 
	    Set<Item> itemSet= new HashSet<Item>();
	    itemSet.add(item1);
	    itemSet.add(item2);
	    itemSet.add(item3);	    
	    cart.setItems(itemSet);
	    cartRepo.save(cart);
	}
	@GetMapping("/student")
	void saveStudent()
	{
		
		  Student s1= new Student();
		  s1.setName("Swami");
		  Student s2= new Student();
		  s2.setName("Vasu");
		  
		  Student s3= new Student();
		  s3.setName("Amit");
		  
		  Set<Student> studentList= new HashSet();
		  studentList.add(s1);
		  studentList.add(s2);
		  studentList.add(s3);
		  
		  
		  Course course1=new Course();
		  course1.setCourseName("Physics");
		  course1.setStudentList(studentList);
		  Course course2=new Course();
		  course2.setCourseName("Maths");
		  course2.setStudentList(studentList);	  
		  Course course3=new Course();
		  course3.setCourseName("Chemsitry");
		  course3.setStudentList(studentList);
		  
		  Set<Course> courseList= new HashSet();
		  courseList.add(course1);
		  courseList.add(course2);
		  courseList.add(course3);
		  s1.setCourse(courseList);
		  Set<Course> courseList1= new HashSet();
		  courseList1.add(course1);
		  courseList1.add(course2);
		  s2.setCourse(courseList1);
		 // courseRepo.save(course3);
		  studentRepo.save(s2);
		  
	}

}
