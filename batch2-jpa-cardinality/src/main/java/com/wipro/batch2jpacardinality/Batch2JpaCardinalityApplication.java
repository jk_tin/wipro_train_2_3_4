package com.wipro.batch2jpacardinality;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2JpaCardinalityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2JpaCardinalityApplication.class, args);
	}

}
