package com.wipro.batch2springtest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch2springtest.controller.service.HomeService;

@RestController
public class HomeController {

  @Autowired 	
  HomeService homeService;
	
	@GetMapping("/hello")
	public String greet()
	{
		return homeService.greet();
		
	}

}
