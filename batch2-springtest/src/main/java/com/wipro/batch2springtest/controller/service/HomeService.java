package com.wipro.batch2springtest.controller.service;

import org.springframework.stereotype.Service;

@Service
public class HomeService {

	public String greet()
	{
		return "Hello World!";
	}
}
