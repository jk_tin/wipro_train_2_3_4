package com.wipro.batch2springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2SpringtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2SpringtestApplication.class, args);
	}

}
