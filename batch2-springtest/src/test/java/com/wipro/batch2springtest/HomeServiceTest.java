package com.wipro.batch2springtest;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.boot.test.context.SpringBootTest; 
import com.wipro.batch2springtest.controller.service.HomeService;
import com.wipro.batch2springtest.controller.*;
@WebMvcTest(HomeController.class)
public class HomeServiceTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private HomeService service;

	@Test
	void greetingShouldReturnMessageFromService() throws Exception {
		when(service
				.greet())
		.thenReturn("Hello World!");
		this
		.mockMvc
		.perform(get("/hello"))
		.andDo(print())
		.andExpect(status().isOk())
				.andExpect(content()
				.string(containsString("Hello World!")));
	}
	
}
