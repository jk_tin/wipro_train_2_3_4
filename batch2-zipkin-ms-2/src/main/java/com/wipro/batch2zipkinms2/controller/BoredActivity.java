package com.wipro.batch2zipkinms2.controller;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.wipro.batch2zipkinms2.dto.Bore;

 

@FeignClient(name = "BoredActivity",
url = "https://www.boredapi.com/api")
public interface BoredActivity {
	
	@GetMapping("/activity")
    public ResponseEntity<Bore>	getBoreActvity();

}
