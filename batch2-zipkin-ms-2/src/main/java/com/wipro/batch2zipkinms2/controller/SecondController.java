package com.wipro.batch2zipkinms2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wipro.batch2zipkinms2.dto.Bore;
import org.springframework.http.ResponseEntity; 

@RestController
@CrossOrigin("*")
public class SecondController {
	Logger logger= LoggerFactory.getLogger(SecondController.class);
    @Autowired
	RestTemplate restTemplate;
    @Autowired
    BoredActivity boredActivity;
    
	@GetMapping("/greet2")
	String greet2()
	{
		logger.info("inside--microservice-2");
		 
		String url="https://www.boredapi.com/api/activity/";
		Bore bore=restTemplate.getForObject(url, Bore.class);
		return bore.toString();
	}
	
	@GetMapping("/greetnew")
	String greetnew()
	{
		 
		ResponseEntity<Bore> response=boredActivity.getBoreActvity();
		
		
		return response.getBody().toString();
	}

}
