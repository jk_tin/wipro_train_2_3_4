package com.wipro.batch2zipkinms2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.wipro.batch2zipkinms2"})
public class Batch2ZipkinMs2Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch2ZipkinMs2Application.class, args);
	}

}
