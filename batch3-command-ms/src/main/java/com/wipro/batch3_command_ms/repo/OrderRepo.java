package com.wipro.batch3_command_ms.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.batch3_command_ms.entity.Order;

public interface OrderRepo extends JpaRepository<Order, Integer> {

}
