package com.wipro.batch3_command_ms.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.wipro.batch3_command_ms.entity.Order;
import com.wipro.batch3_command_ms.repo.OrderRepo;

@Component
public class CommandHandler {
	@Autowired
	OrderRepo orderRepo;
	@Autowired
	KafkaTemplate<String,Order> kafkaTemplate;
	
	public void handle(Order order)
	{
		Order data= new Order();
		data.setOrderId(order.getOrderId());
		data.setOrderStatus(order.getOrderStatus());
		orderRepo.save(order);
		kafkaTemplate.send("order-topic",order);
	}

}
