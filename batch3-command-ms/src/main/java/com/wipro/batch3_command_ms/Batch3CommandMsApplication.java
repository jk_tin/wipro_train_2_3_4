package com.wipro.batch3_command_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3CommandMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3CommandMsApplication.class, args);
	}

}
