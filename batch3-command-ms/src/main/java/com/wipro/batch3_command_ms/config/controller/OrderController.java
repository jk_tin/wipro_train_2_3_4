package com.wipro.batch3_command_ms.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3_command_ms.entity.Order;
import com.wipro.batch3_command_ms.handler.CommandHandler;

@RestController
public class OrderController {
	@Autowired
	CommandHandler commandHandler;
	
	@PostMapping("/order")
	void sendData(@RequestBody Order order)
	{
		commandHandler.handle(order);
		
	}

}
