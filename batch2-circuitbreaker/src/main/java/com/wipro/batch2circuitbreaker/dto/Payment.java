package com.wipro.batch2circuitbreaker.dto;
 
 
public class Payment {
	
	 
	int id;
	
	 
	double amount;
	
	 
	double ordrId;
	
	 
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getOrdrId() {
		return ordrId;
	}

	public void setOrdrId(double ordrId) {
		this.ordrId = ordrId;
	}

	@Override
	public String toString() {
		return "Payment [id=" + id + ", amount=" + amount + ", ordrId=" + ordrId + "]";
	}

	 
 

}
