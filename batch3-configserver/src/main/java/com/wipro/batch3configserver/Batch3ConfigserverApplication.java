package com.wipro.batch3configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3ConfigserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3ConfigserverApplication.class, args);
	}

}
