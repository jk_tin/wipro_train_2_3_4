package com.wipro.batch2serviceregisrty;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Batch2ServiceRegisrtyApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2ServiceRegisrtyApplication.class, args);
	}

}
