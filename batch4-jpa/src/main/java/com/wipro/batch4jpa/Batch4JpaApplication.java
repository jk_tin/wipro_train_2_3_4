package com.wipro.batch4jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

 
 
@SpringBootApplication
public class Batch4JpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4JpaApplication.class, args);
	}

}
