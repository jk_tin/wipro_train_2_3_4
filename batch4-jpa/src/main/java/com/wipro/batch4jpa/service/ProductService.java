package com.wipro.batch4jpa.service;
import java.util.List;

import org.springframework.data.domain.Page;

import com.wipro.batch4jpa.entity.*;

public interface ProductService {
	
	List<Product> findAll();
	Product  findById(int id);
	void save(Product product);
	void update(Product product);
	void deleteById(int id);
	List<Product> findByProdCatOrderByIdDesc(String catName);
	List<Product> findByProdCatAndPriceOrderByIdDesc(String catName,String price);
	public Page<Product> findAllPageable();
	
}
