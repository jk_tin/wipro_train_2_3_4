package com.wipro.batch4jpa.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.wipro.batch4jpa.entity.Product;
import com.wipro.batch4jpa.repo.ProductRepo;
import com.wipro.batch4jpa.service.ProductService;
 

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	ProductRepo productRepo;
	
	@Override
	public List<Product> findAll() {
		 
		return productRepo.findAll();
	}

	@Override
	public Product findById(int id) {
		 
		return productRepo.findById(id).get();
	}

	@Override
	public void save(Product product) {
		 
		productRepo.save(product);
	}

	@Override
	public void update(Product product) {
		productRepo.save(product);
		
	}

	@Override
	public void deleteById(int id) {
		productRepo.deleteById(id);
		
	}

	@Override
	public List<Product> findByProdCatOrderByIdDesc(String catName) {
		// TODO Auto-generated method stub
		return productRepo.findByProdCatOrderByIdDesc(catName);
	}

	@Override
	public List<Product> findByProdCatAndPriceOrderByIdDesc(String catName, String price) {
		// TODO Auto-generated method stub
		return productRepo.findByProdCatAndPriceOrderByIdDesc( catName, price);
	}
	
	
	@Override
	public Page<Product> findAllPageable() {
		// TODO Auto-generated method stub
		Sort sort = Sort.by(Sort.Direction.DESC, "prodName");
	    Pageable pageable = PageRequest.of(0, 2, sort);
		return productRepo.findAll(pageable);
	}

}
