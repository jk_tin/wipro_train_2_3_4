package com.wipro.batch4jpa.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4jpa.entity.Product;

@Repository
public interface ProductRepo extends JpaRepository<Product, Integer> {

	List<Product> findByProdCatOrderByIdDesc(String caregoryName);
	List<Product> findByProdCatAndPriceOrderByIdDesc(String caregoryName,String price);
}
