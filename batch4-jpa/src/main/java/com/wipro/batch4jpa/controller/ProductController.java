package com.wipro.batch4jpa.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4jpa.entity.Product;
import com.wipro.batch4jpa.service.ProductService;
 
 
@RestController
public class ProductController {
	@Autowired
	ProductService productService;
	 
	@GetMapping("/product")
	List<Product> findAll()
	{
		return productService.findAll();
		
	}
	
	@GetMapping("/product/{id}")
	Product findById(@PathVariable int id)
	{
		return productService.findById(id);
		
	}
	
	@PostMapping("/product")
	void save(@RequestBody Product product)
	{
		  productService.save(product);
		
	}
	
	@PutMapping("/product")
	void update(@RequestBody Product product)
	{
		  productService.save(product);
		
	}
	

	@DeleteMapping("/product/{id}")
	void deleteById(@PathVariable int  id)
	{
		  productService.deleteById(id);
		
	}

	@PostMapping("/prodcat")
	List<Product> findByCategory(@RequestBody Product product)
	{
		System.out.println(product.getProdCat());
		return productService.findByProdCatOrderByIdDesc(product.getProdCat());
		
	}
	@PostMapping("/prodcatprice")
	List<Product> findByProdCatAndPriceOrderByIdDesc(@RequestBody Product product)
	{
		System.out.println(product.getProdCat());
		return productService.findByProdCatAndPriceOrderByIdDesc(product.getProdCat(),product.getPrice());
		
	}
	@GetMapping("/productpg")
	Page<Product> findAllPageable()
	{
		return productService.findAllPageable();
		
	}
}
