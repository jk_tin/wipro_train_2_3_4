package com.wipro.batch4_configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4ConfigserverApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4ConfigserverApplication.class, args);
	}

}
