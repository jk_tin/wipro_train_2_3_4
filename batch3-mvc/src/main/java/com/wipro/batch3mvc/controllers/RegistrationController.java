package com.wipro.batch3mvc.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.wipro.batch3mvc.dto.*;
import java.util.*;
@Controller
public class RegistrationController {
	
	@GetMapping("/")
	String home()
	{
		return "index";
		
	}
	
	@GetMapping("/register")
	String register()
	{
		return "register";
		
	}
	
	@GetMapping("/register_start")
	String registerStart(Model m)
	{
		User user= new User();		
		m.addAttribute("user", user);
		List<String> listProfession = new ArrayList();
		listProfession.add("Banker");
		listProfession.add("Engineer");
		listProfession.add("Doctor");
		m.addAttribute("listProfession", listProfession);
		return "reg_proc";
		
	}
	
	@PostMapping("/register_done")
	String registrationDone (@ModelAttribute("user") User user)
	{
		 
		return "reg_done";
		
	}

}
