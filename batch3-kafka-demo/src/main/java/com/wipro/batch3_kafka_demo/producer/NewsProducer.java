package com.wipro.batch3_kafka_demo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import com.wipro.batch3_kafka_demo.dto.*;
@Component
public class NewsProducer {

    @Autowired
    private KafkaTemplate<String, News> kafkaTemplate;

    public void sendMessage(String topic, News news) {
        kafkaTemplate.send(topic, news);
    }

}
