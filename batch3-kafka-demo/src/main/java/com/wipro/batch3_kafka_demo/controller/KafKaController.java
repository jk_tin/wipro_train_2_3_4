package com.wipro.batch3_kafka_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3_kafka_demo.dto.DataDTO;
import com.wipro.batch3_kafka_demo.dto.News;
import com.wipro.batch3_kafka_demo.producer.MessageProducer;
import com.wipro.batch3_kafka_demo.producer.NewsProducer;

@RestController
public class KafKaController {
	@Autowired
	MessageProducer messageProducer;
	
	@Autowired
	NewsProducer newsProducer;
	
	@PostMapping("/send")
	void sendMessage(@RequestBody DataDTO dataDTO)
	{
		messageProducer.sendMessage(dataDTO.getTopic(),dataDTO.getMessage());
		
	}
	
	@PostMapping("/news")
	void sendNews(@RequestBody News news)
	{
		newsProducer.sendMessage("breaking-news-latest",news);
		
	}

}
