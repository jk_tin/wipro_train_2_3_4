package com.wipro.batch3_kafka_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3KafkaDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3KafkaDemoApplication.class, args);
	}

}
