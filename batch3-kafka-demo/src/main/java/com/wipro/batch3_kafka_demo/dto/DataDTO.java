package com.wipro.batch3_kafka_demo.dto;

public class DataDTO {
	
	
	String topic;
	String message;
	public String getTopic() {
		return topic;
	}
	public void setTopic(String topic) {
		this.topic = topic;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "DataDTO [topic=" + topic + ", message=" + message + "]";
	}
	
	

}
