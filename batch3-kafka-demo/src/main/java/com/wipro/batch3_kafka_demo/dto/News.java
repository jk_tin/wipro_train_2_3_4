package com.wipro.batch3_kafka_demo.dto;

public class News {

	String category;
	String headline;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getHeadline() {
		return headline;
	}
	public void setHeadline(String headline) {
		this.headline = headline;
	}
	
	
	
}
