package com.wipro.batch3_kafka_demo.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

    @KafkaListener(topics = "last-min-deal", groupId = "batch3-group-id")
    public void listen(String message) {
        System.out.println("Received message: " + message);
    }

}