package com.wipro.batch4_command_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4CommandMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4CommandMsApplication.class, args);
	}

}
