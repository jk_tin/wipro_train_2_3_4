package com.wipro.batch4_command_ms.hadler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.wipro.batch4_command_ms.entity.Order;
import com.wipro.batch4_command_ms.repo.OrderRepo;

@Service
public class OrderHandler {
	
	@Autowired
	OrderRepo orderRepo;
	@Autowired
	KafkaTemplate kafkaTemplate;
	
	public void handle(Order order)
	{
		
		Order orderNew = new Order();
		orderNew.setAmount(order.getAmount());
		orderNew.setOrderId(order.getOrderId());
		orderNew.setStatus(order.getStatus());
		orderRepo.save(orderNew);
		kafkaTemplate.send("order-msg",order);
		
		
	}

}
