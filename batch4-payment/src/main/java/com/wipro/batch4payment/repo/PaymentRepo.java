package com.wipro.batch4payment.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.batch4payment.entity.Payment;

public interface PaymentRepo extends JpaRepository<Payment, Integer> {

}
