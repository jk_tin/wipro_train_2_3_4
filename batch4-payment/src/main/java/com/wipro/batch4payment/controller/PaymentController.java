package com.wipro.batch4payment.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4payment.repo.PaymentRepo;
import java.util.*;
import com.wipro.batch4payment.entity.*;
@RestController
public class PaymentController {
	@Autowired
	PaymentRepo paymentRepo;
	
	@GetMapping("/payment")
	List<Payment> findAll()
	{
		return paymentRepo.findAll();
		
	}
	
	@GetMapping("/payment/{id}")
	Payment findById(@PathVariable int id)
	{
		return paymentRepo.findById(id).get();
		
	}
	
	@PostMapping("/payment")
	void save(@RequestBody Payment payment)
	{
		 paymentRepo.save(payment);
		
	}

}
