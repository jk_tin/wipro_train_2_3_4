package com.wipro.batch4order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wipro.batch4order.repo.OrderRepo;
import com.wipro.batch4order.vo.*;

import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;

import com.wipro.batch4order.entity.*;
@RestController
public class OrderController {
	
	@Autowired
	OrderRepo orderRepo;
	
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/find")
	OrderVO find()
	{
		OrderVO orderVO= new OrderVO();
		Order order= new Order();
		Payment payment=new Payment();
		orderVO.setOrder(order);
		orderVO.setPayment(payment);
		return orderVO;
		
	}
	
	@PostMapping("/order")
	@CircuitBreaker(name = "orderCircuitBreaker", fallbackMethod = "showServiceDown")
	String save(@RequestBody OrderVO orderVO)
	{
		String returnValue="";
		Order order=orderVO.getOrder();
		Payment payment= orderVO.getPayment();
		order=orderRepo.save(order);
		payment.setOrderId(order.getId());
		//String url="http://localhost:9001/payment";
		String url="http://BATCH4-PAYMNET-MS/payment";
		ResponseEntity<Object> response=restTemplate.postForEntity(url, payment, Object.class);
		if(response.getStatusCode().value()==200)
		{
			returnValue="Completed";
			order.setOrderStatus(returnValue);
			 
		}
		else		
		{
			returnValue="Failed";
			order.setOrderStatus(returnValue);
		}
		orderRepo.save(order);
		return returnValue;
	}
	
	String showServiceDown(Throwable t)
	{
		System.out.println("System is down");
		
		return "System is down";
	}
	@GetMapping("/order/{id}")
	Order findById(@PathVariable int id)
	{
		return orderRepo.findById(id).get();
		//return orderVO;
		
	}
	

}
