package com.wipro.batch4order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4OrderApplication.class, args);
	}

}
