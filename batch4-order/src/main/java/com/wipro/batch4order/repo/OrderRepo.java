package com.wipro.batch4order.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4order.entity.*;

@Repository
public interface OrderRepo extends JpaRepository<Order,Integer>{

}
