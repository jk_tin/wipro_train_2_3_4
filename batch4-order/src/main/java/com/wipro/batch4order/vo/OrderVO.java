package com.wipro.batch4order.vo;

import com.wipro.batch4order.entity.Order;

public class OrderVO {
	
	private Order order;
	private Payment payment;
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	@Override
	public String toString() {
		return "OrderVO [order=" + order + ", payment=" + payment + "]";
	}

}
