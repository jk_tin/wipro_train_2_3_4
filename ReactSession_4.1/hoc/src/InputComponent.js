 
import './App.css';
 
const InputComponent = ({ type, isHovered }) => {
  return (
    <>
      <input
      type={type}
      style={{ backgroundColor: isHovered ? "blue" : "white" }}
    />
    </>
  );
};

export default InputComponent;
