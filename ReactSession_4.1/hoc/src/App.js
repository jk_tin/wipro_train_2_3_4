import './App.css';
import TextComponent from './TextComponent';
import withHover from './Hoc'
import InputComponent from './InputComponent';
function App() {
  
  const TextComponentWithHover = withHover(TextComponent);
  const InputComponentWithHover = withHover(InputComponent);

  return (
     <>
     <TextComponentWithHover text="This is my Text"></TextComponentWithHover>
     <InputComponentWithHover type="text"></InputComponentWithHover>
     </>
  );
}

export default App;
