import react from 'react'
import { Component } from 'react';
import './App.css';

class App extends Component {

  constructor(props) {
    console.log("--constructor--");
    super(props);
    this.state = {favoritefood: "rice"};
  }
  shouldComponentUpdate(nextProps, nextState) {
    // Only re-render if the favoriteFood state has changed
    console.log("--shouldComponentUpdate---"+this.state.favoritefood+"--")
    return this.state.favoritefood !== nextState.favoriteFood;
  }
  componentDidMount() {
    setTimeout(() => {
      this.setState({favoritefood: "pizza"})
    }, 2000)
  }
  // static getDerivedStateFromProps(props, state) {
  //   return {favoritefood: props.favfod };
  // }
  render() {
    console.log("--Render--");
    return (
      <div>
         
        <h1>My favourite food : {this.state.favoritefood}</h1>
      </div>
    );
  }
}

export default App;
