
import './App.css';
import Products from './Products';
import Home from './Home';
import About from './About';
import { Routes, Route } from 'react-router-dom';
import Navbar from './Navbar';
import NoMatch from './NoMatch';
import OrderSummary from './OrderSummary';
function App() {
  return (
     <>
      <Navbar/>
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products/:param/:param1" element={<Products />} />
          <Route path="/about" element={<About />} />
          <Route path="/order-summary" element={<OrderSummary />} />
          <Route path="*" element={<NoMatch />} />
       </Routes>

     
     </>
  );
}

export default App;
