import './App.css'
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
function Products() {
  
  const navigate = useNavigate();
  const {param}=useParams();
  const {param1}=useParams();
  return (
     <>
      <h1>Products-{param}-{param1}</h1>
      <button onClick={()=>navigate('/order-summary')}>Order Summary</button>
     </>
  );
}

export default Products;
