 
import './App.css';
import { useContext } from 'react';
import { MyContext } from './MyContext';

function Cart() {
   const { count, setCount } = useContext(MyContext);
  

 return (
    <>
     <h1>This is Cart-{count}</h1>
    </>
 );
}

export default Cart;
