 
import './App.css';
import PropTypes from "prop-types";

function Employee({name,age}) {

   

 return (
    <>
        <h1> Employee Name : {name}</h1>
        <h1> Employee Age  : {age}</h1>

    </>
 );
}
Employee.propTypes = {
  name: PropTypes.string,    
  age: PropTypes.number,
  
};


export default Employee;
