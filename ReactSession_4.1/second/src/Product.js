 
import './App.css';
import { useContext } from 'react';
import { MyContext } from './MyContext';

function Product() {
   const { count, setCount } = useContext(MyContext);
  
  function addToCart()
  {
    setCount(count+1);

  }
 return (
    <>
      <h1>This is Product {count}</h1>
      <button onClick={addToCart}>Add To Cart</button>
    </>
 );
}

export default Product;
