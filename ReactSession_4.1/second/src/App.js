 
import './App.css';
import Product from './Product';
import Cart from './Cart';
import { useState } from 'react';

import { MyContext } from './MyContext';

function App() {
  
 const [count,setCount]=useState(0);

 return (
    <>
     <MyContext.Provider value={{ count, setCount }}>
     <Product></Product>
     <Cart></Cart>
     </MyContext.Provider>

    </>
 );
}

export default App;
