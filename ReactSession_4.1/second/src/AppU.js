import logo from './logo.svg';
import './App.css';
import {useRef,useEffect} from 'react'
//uncontrolled example
function App() {

  const elementRef = useRef();

  useEffect(() => {
   const divElement = elementRef.current;
   console.log(divElement); // logs <div>I'm an element</div>
 }, []);

 return (
   <div ref={elementRef}>
     I'm an element
   </div>
 );
}

export default App;
