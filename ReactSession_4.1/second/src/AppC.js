import logo from './logo.svg';
import './App.css';
import {useState} from 'react'

function AppC() {

  const [name,setName]=useState("Jayanta");

  function handleClick()
  {
    setName("Amit");

  }

  return (
     <>
      <h1>Second App-{name}</h1>

      <button onClick={handleClick}>Click to change</button>

      </>
  );
}

export default App;
