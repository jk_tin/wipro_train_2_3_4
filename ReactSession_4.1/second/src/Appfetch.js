 
import './App.css';

import { useEffect, useState } from 'react';

function App() {
  
  const [user,setUser]=useState(null);

  useEffect(() => {
   const ftch=fetch("http://localhost:8000/users");
   ftch.then((resp)=>resp.json())
   .then((res)=>{
        setUser(res);
       console.log(res);
   });
  }, []);
  

 return (
    <>
       <ul>
      {
      user.map(u => (
        <li key={u.id}>{u.name}</li>
      ))}
    </ul>
    
    </>
 );
}

export default App;
