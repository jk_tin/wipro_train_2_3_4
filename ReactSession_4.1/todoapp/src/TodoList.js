 
import TodoItem from "./TodoItem";

function TodoList({items,handleEdit,handleDelete}) {
  
  return (
    <>
           <ul className="list-group my-2">
        {
            items.map((item) => {
            
            return <TodoItem item={item} handleEdit={handleEdit} handleDelete={handleDelete} />;
        })}
      </ul>
    </>
  );
}

export default TodoList;
