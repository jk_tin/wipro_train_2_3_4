import { useState } from "react";
import TodoInput from "./TodoInput";
import {useEffect} from 'react';
import TodoList from "./TodoList";
function App() {
  const[items,setItems]=useState([]);
  const [editItem, setEditItem] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [deleteFlag, setDeleteFlag] = useState(false);
  
  const [activeItem, setActiveItem] = useState({
    id: "",
    title: "",
    completed: false,
  });

  function handleChange(e) {
    const { name, value } = e.target;
    setActiveItem((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  }

  useEffect(() => {
    const ftch=fetch("http://localhost:8000/todo");
    ftch
    .then((resp)=>resp.json())
    .then((res)=>{
        setItems(res);
        console.log(res);
    });
   }, [deleteFlag]);

  function handleSubmit(item) {
   
    
    if(item.id==='') //add 
    {
    
      fetch("http://localhost:8000/todo", {
        // Replace with your endpoint URL
        method: "POST",
        headers: { "Content-Type": "application/json" }, // Specify JSON content type
        body: JSON.stringify({ title: item.title, completed: item.completed }), // Convert form data to JSON
      })
        .then((response) => response.json())
        .then((json) => {
          //console.log(json);
        });
      }
      else{

        fetch("http://localhost:8000/todo/"+item.id, {
          // Replace with your endpoint URL
          method: "PUT",
          headers: { "Content-Type": "application/json" }, // Specify JSON content type
          body: JSON.stringify({ title: item.title, completed: item.completed }), // Convert form data to JSON
        })
          .then((response) => response.json())
          .then((json) => {
            //console.log(json);
          });

      }


  }

  function handleEdit(item)
  {

     console.log("Edit clicked--");
     setEditItem(true);
     setEditMode(true);
     fetch("http://localhost:8000/todo/" + item.id, {
      // Replace with your endpoint URL
      method: "GET",
    })
      .then((response) => response.json())
      .then((json) => {
        setActiveItem(item);
        console.log(json);
      });


  }

  function handleDelete(item)
  {
    setDeleteFlag(false);
    fetch("http://localhost:8000/todo/"+item.id, {
      // Replace with your endpoint URL
      method: "DELETE"
     
    })
      .then((response) => response.json())
      .then((json) => {        
          setDeleteFlag(true);
       });


  }

  return (
    <>
      <div className="container">
      <h1 className="text-uppercase text-center my-2">Todo App</h1>
      <div className="row">
        <div className="col-8 col-md-6 mx-auto mt-2">
          <TodoInput
            activeItem={activeItem}
            editItem={editItem}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
          />
          <TodoList
            items={items}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
          />
        </div>
      </div>
    </div>
    </>
  );
}

export default App;
