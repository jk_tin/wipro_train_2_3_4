import { useFormik } from "formik";
import React from "react";
import * as Yup from "yup";
const SignupForm = () => {
  // Pass the useFormik() hook initial form values and a submit function that will
  // be called when the form is submitted
  const formik = useFormik({
    initialValues: {
      fname: "",
      lname: "",
      email: "",
    },
    onSubmit: (values) => {
      console.log(JSON.stringify(values));
    },
    validationSchema: Yup.object({
      fname: Yup.string()
        .max(7, "Must be 7 characters or less")
        .required("Required"),
      lname: Yup.string()
        .max(3, "Must be 3 characters or less")
        .required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
    }),
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <label htmlFor="fname">First Name</label>
      <input
        id="fname"
        name="fname"
        type="text"
        onChange={formik.handleChange}
        value={formik.values.fname}
        onBlur={formik.handleBlur}
      />
      {formik.touched.fname && formik.errors.fname ? (
        <div>{formik.errors.fname}</div>
      ) : null}
      <label htmlFor="lname">Last Name</label>
      <input
        id="lname"
        name="lname"
        type="lname"
        onChange={formik.handleChange}
        value={formik.values.lname}
        onBlur={formik.handleBlur}
      />
      {formik.touched.lname && formik.errors.lname ? (
        <div>{formik.errors.lname}</div>
      ) : null}
      <label htmlFor="email">Email</label>
      <input
        id="email"
        name="email"
        type="email"
        onChange={formik.handleChange}
        value={formik.values.email}
        onBlur={formik.handleBlur}
      />
      {formik.touched.email && formik.errors.email ? (
        <div>{formik.errors.email}</div>
      ) : null}
      <button type="submit">Submit</button>
    </form>
  );
};

export default SignupForm;
