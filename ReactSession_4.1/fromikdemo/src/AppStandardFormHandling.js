 
import './App.css';
import { useState } from 'react';
function App() {

  const [fName,setFName]=useState("");

  function changeHandler(e)
  {
    setFName(e.target.value);

  }
  function changeText()
  {
    setFName("Text Changed");

  }


  return (
     <>

        FirstName : <input type="text" value={fName} onChange={changeHandler}></input>
        <button onClick={changeText}>Change Text</button>
        {fName}
     </>
  );
}

export default App;
