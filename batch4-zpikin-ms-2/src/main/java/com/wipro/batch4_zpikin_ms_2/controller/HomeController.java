package com.wipro.batch4_zpikin_ms_2.controller;

import org.apache.commons.logging.LogFactory;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
 

@RestController
public class HomeController {
	
 
	
	Logger logger= LoggerFactory.getLogger(HomeController.class);
	
	@GetMapping("/greet")
	String greet()
	{
		logger.info("--Logging From MS2--");
		return "Hello ZipKin";
	}
	@GetMapping("/greet2")
	String greetSecond()
	{
		logger.info("--Logging From MS2--");
		return "Hello ZipKin-2";
	}

}
