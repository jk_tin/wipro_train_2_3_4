package com.wipro.batch4_zpikin_ms_2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4ZpikinMs2Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch4ZpikinMs2Application.class, args);
	}

}
