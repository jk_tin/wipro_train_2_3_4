import logo from './logo.svg';
import './App.css';
import {useEffect} from 'react'
import {useState} from 'react'

function App() {

  const [users,setUsers]=useState();

  useEffect(() => {
    fetch('http://localhost:8000/users')
       .then((response) => response.json())
       .then((data) => {
          console.log(data);
          setUsers(data);
       })
       .catch((err) => {
          console.log(err.message);
       });
 }, []);


 return (
  <div className="posts-container">
     {users?.map((user) => {
        return (
           <div className="post-card" key={user.id}>
              <h2 className="post-title">{user.name}</h2>  
              <h2 className="post-title">{user.email}</h2>             
              <h2 className="post-title">{user.number}</h2>   
              <div className="button">
              <div className="delete-btn">Delete</div>
              </div>
           </div>
        );
     })}
  </div>
  );
}

export default App;
