import logo from './logo.svg';
import './App.css';
import {useEffect} from 'react'
import {useState} from 'react'

function App() {

  const [id,setId]=useState("");
  const [name,setName]=useState("");
  const [email,setEmail]=useState(""); 
  const [number,setNumber]=useState(""); 

  async function handleClick()
  {
    await fetch('http://localhost:8000/users', {
      method: 'POST',
      body: JSON.stringify({
         id: id,
         name: name,
         email: email,
         number:number
      }),
      headers: {
         'Content-type': 'application/json; charset=UTF-8',
      },
      })
      .then((response) => response.json())
      .then((data) => {
        console.log("--here");
        setEmail("");
        setId("");
        setName("");
        setNumber("");
          
      })
      .catch((err) => {
         console.log(err.message);
      });

  }

 return (
  <div className="posts-container">
   
   Id:<input type="text" value={id} onChange={(e)=>setId(e.target.value)} />
   Name:<input type="text" value={name} onChange={(e)=>setName(e.target.value)} />       
   Email:<input type="text" value={email} onChange={(e)=>setEmail(e.target.value)} />       
   Number:<input type="text" value={number} onChange={(e)=>setNumber(e.target.value)} />  

   <button onClick={handleClick}>Save Data</button>        
     
  </div>
  );
}

export default App;
