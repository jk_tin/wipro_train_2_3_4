import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
function App() {
 
//  let count=0;
 const [count,setCount]= useState(0);
 const[name,setName]=useState("");
 const[age,setAge]=useState(21);
 function handleClick()
 {
     setCount(count+1);
 }

//  function changeValue(e)
//  {
//   //console.log(e.target.value);
//   setName(e.target.value)
//  }

  return (
     <>
      <p>Hello World!!!{count}</p>
      <button onClick={handleClick}>Click me </button>
      <input type="text"  onChange={(e)=>setName(e.target.value)} />
      <input type="number"  onChange={(e)=>setAge(e.target.value)} />
      <p>{name}</p>
      <p>{age}</p>
     </>
  );
}

export default App;
