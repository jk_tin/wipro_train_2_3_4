import logo from './logo.svg';
import './App.css';
import { useState, React } from "react";
import { MyContext } from "./MyContext";
import MyComponent from "./MyComponent";
import MyComponent2 from "./MyComponent2";
function App() {
  const [text, setText] = useState("");
  return (
    <div className="App">
       <MyContext.Provider value={{ text, setText }}>
        <MyComponent />
        <MyComponent2 />
      </MyContext.Provider>
    </div>
  );
}

export default App;
