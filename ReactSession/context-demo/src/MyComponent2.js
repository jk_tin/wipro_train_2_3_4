import { useContext } from 'react';
import { MyContext } from './MyContext';

function MyComponent2() {
  const { text, setText } = useContext(MyContext);

  return (
    <div>
      <h1>Text is : {text}</h1>
       
      
    </div>
  );
}

export default MyComponent2;