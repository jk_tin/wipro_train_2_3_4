// Products.js
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
const Products = () => {
 const navigate = useNavigate();
 const { productId,catId } = useParams();
 return (
    <div className="container">
       <div className="title">
          <h1>Order Product CockTails</h1>
          <h2>Product Details for ID: {productId}</h2>
          <h2>Product Details Category: {catId}</h2>
       </div>
       <button className="btn" onClick={() => navigate('OrderSummary')}>
          Place Order
       </button>
    </div>
 );
};

export default Products;