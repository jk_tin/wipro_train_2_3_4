// App.js
import { Routes, Route } from 'react-router-dom';
import Home from './Home';
import About from './About';
import Products from './Products';
import NavBar from './NavBar';
import Formt from './Formt';
const App = () => {
 return (
    <>
      <NavBar />
       <Routes>
          <Route path="/" element={<Home />} />  
          <Route path="/about" element={<About />} />
          <Route path="/products/:productId/:catId" element={<Products/>} />
          <Route path="/form" element={<Formt/>} />
       </Routes>
    </>
 );
};

export default App;