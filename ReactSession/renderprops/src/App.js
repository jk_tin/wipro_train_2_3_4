import './App.css';
import Button from './Button';

function App() {

  function handleClick1()
  {
    console.log("--HandleClick -1 called");

  }

  function handleClick2()
  {
    console.log("--HandleClick -2 called");

  }

  function render1()
  {

    return "Button-1"
  }

  function render2()
  {

    return "Button-2"
  }


  return (
     <>
      <Button render={render1} onClick={handleClick1}></Button>
      <Button render={render2} onClick={handleClick2}></Button>
     </>
  );
}

export default App;
