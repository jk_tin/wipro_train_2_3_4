import React, { Component } from 'react';

class App extends Component{

    constructor(props) {
        super(props);
        this.state = {favoritefood: "rice"};
        console.log("-constructor-")
      }

    render() {
        console.log("-render-")
        return (
            <>
          <h1>My Favorite Food is {this.state.favoritefood} </h1>
          <button type="button" onClick={this.changeFood}>Change food</button>
          </>
        );
      }
      shouldComponentUpdate(nextProps, nextState) {
        // Only re-render if the favoriteFood state has changed
        console.log("this.state.favoriteFood="+this.state);
       // console.log("this.nextState.favoriteFood="+this.nextState.favoriteFood);
        return this.state.favoriteFood == nextState.favoriteFood;
      }
    
      componentDidMount() {
        console.log("-componentDidMount-")
        setTimeout(() => {
          this.setState({favoritefood: "pizza"})
        }, 1000)
      }


}
export default App;