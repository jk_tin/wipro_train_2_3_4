import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
function Signup() {
  const formik = useFormik({
    initialValues: {
      email: "",
      fname: "",
      lname: "",
    },
    validationSchema: Yup.object({
      fname: Yup.string()
        .max(10, "Must be 10 characters or less")
        .required("Required"),
      lname: Yup.string()
        .max(10, "Must be 10 characters or less")
        .required("Required"),
      email: Yup.string().email("Invalid email address").required("Required"),
    }),
    onSubmit: (values) => {
      alert(JSON.stringify(values, null, 2));
    },
  });

  return (
    <>
      <form onSubmit={formik.handleSubmit}>
        <label htmlFor="email">Email Address:</label>
        <input
          id="email"
          name="email"
          type="email"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.email}
        />
        {formik.touched.email && formik.errors.email ? (
          <div>{formik.errors.email}</div>
        ) : null}
        <label htmlFor="firstname">First Name:</label>
        <input
          id="fname"
          name="fname"
          type="text"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.fname}
        />
        {formik.touched.fname && formik.errors.fname ? (
          <div>{formik.errors.fname}</div>
        ) : null}

        <label htmlFor="lastname">Last Name:</label>
        <input
          id="lname"
          name="lname"
          type="text"
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          value={formik.values.lname}
        />
        {formik.touched.lname && formik.errors.lname ? (
          <div>{formik.errors.lname}</div>
        ) : null}

        <button type="submit">Submit</button>
      </form>
    </>
  );
}

export default Signup;
