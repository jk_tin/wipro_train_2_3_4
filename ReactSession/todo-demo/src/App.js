import "./App.css";
import TodoInput from "./TodoInput";
import TodoList from "./TodoList";
import { useEffect } from "react";
import { useState } from "react";
function App() {
  const [items, setItems] = useState([]);
  const [deleteFlag, setDeleteFlag] = useState(false);
  const [editMode, setEditMode] = useState(false);
  const [activeItem, setActiveItem] = useState({
    id: "",
    title: "",
    completed: false,
  });
  useEffect(() => {
    console.log("--useEffect--")
    fetch("http://localhost:3000/todo", {
      // Replace with your endpoint URL
      method: "GET",
    })
      .then((response) => response.json())
      .then((json) => {
        setItems(json);
        console.log(json);
      });
  }, [deleteFlag]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    setActiveItem((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };
  const handleEdit = (item) => {
    setEditMode(true);
    alert("Edit :: " + JSON.stringify(item));
    fetch("http://localhost:3000/todo/" + item.id, {
      // Replace with your endpoint URL
      method: "GET",
    })
      .then((response) => response.json())
      .then((json) => {
        setActiveItem(item);
        console.log(json);
      });
  };
  const handleDelete = (item) => {
    setDeleteFlag(false);
    fetch("http://localhost:3000/todo/" + item.id, {
      // Replace with your endpoint URL
      method: "DELETE",
    })
      .then((response) => response.json())
      .then((json) => {
        console.log(json);
        setDeleteFlag(true);
      });
  };
  const handleSubmit = (item) => {
    // alert(item.id);
    if (item.id !== "") {
      fetch("http://localhost:3000/todo/"+item.id, {
        // Replace with your endpoint URL
        method: "PUT",
        headers: { "Content-Type": "application/json" }, // Specify JSON content type
        body: JSON.stringify({ title: item.title, completed: item.completed }), // Convert form data to JSON
      })
        .then((response) => response.json())
        .then((json) => {
          console.log(json);
        });
      
    } else {
      fetch("http://localhost:3000/todo", {
        // Replace with your endpoint URL
        method: "POST",
        headers: { "Content-Type": "application/json" }, // Specify JSON content type
        body: JSON.stringify({ title: item.title, completed: item.completed }), // Convert form data to JSON
      })
        .then((response) => response.json())
        .then((json) => {
          console.log(json);
        });
    }

    setEditMode(false);
  };
  return (
    <div className="container">
      <h1 className="text-uppercase text-center my-2">Todo App</h1>
      <div className="row">
        <div className="col-8 col-md-6 mx-auto mt-2">
          <TodoInput
            activeItem={activeItem}
            editItem={editMode}
            handleChange={handleChange}
            handleSubmit={handleSubmit}
          />
          <TodoList
            items={items}
            handleEdit={handleEdit}
            handleDelete={handleDelete}
          />
        </div>
      </div>
    </div>
  );
}

export default App;
