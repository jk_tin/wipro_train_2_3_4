import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './Home';
import About from './About';

import { useLocation } from 'react-router-dom';
import Navbar from './Navbar';

function App() {
  const location = useLocation();

  return (
    <>
    <Navbar> </Navbar>
    <BrowserRouter>
      <div className="route-container">
        <Routes location={location}> {/* Pass location for animation control */}
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
 
}
 

export default App;
