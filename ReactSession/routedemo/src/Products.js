import logo from './logo.svg';
import './App.css';
import { useNavigate } from 'react-router-dom';
function Products() {

  const navigate = useNavigate();
  return (
     <>      
       <p>This is Products!!!</p>     
       <button onClick={() => navigate('/`order-summary')}>Order Summary</button>
     </>
  );
}

export default Products;
