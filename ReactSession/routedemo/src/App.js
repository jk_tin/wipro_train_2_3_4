import logo from './logo.svg';
import './App.css';
import { Routes, Route } from 'react-router-dom';
import Home from './Home';
import Products from './Products';
import About from './About';
import Navbar from './Navbar';
import Nomatch from './Nomatch';
import OrderSummary from './OrderSummary';

function App() {
  return (
     <>
     <Navbar></Navbar>
      <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/products" element={<Products />} />
          <Route path="/about" element={<About />} />
          <Route path="/order-summary" element={<OrderSummary/>} />
          <Route path="*" element={<Nomatch />} />
       </Routes>

     
     </>
  );
}

export default App;
