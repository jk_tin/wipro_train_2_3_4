import logo from './logo.svg';
import './App.css';
import { useState } from 'react';
import { MyContext } from './MyContext';
import MyComponent from './MyComponent';
import SecondComponent from './SecondComponet'
function App() {

  const [count,setCount]=useState(0);


  return (
   <>
   <MyContext.Provider value={{ count, setCount }}>

    <MyComponent></MyComponent>
    <SecondComponent></SecondComponent>
    </MyContext.Provider>
   
   
   </>
  );
}

export default App;
