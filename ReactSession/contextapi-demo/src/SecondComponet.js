import { MyContext } from "./MyContext";
import { useContext } from "react";
function SecondComponent()
{

    const { count, setCount } = useContext(MyContext);

    return(

        <>
            <h1>Second Component</h1>
            <h2> The current Count is :{count}</h2>
        </>

    )

}
export default SecondComponent;