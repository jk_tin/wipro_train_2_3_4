import { useContext } from "react";
import { MyContext } from "./MyContext";


function MyComponent(){
    const { count, setCount } = useContext(MyContext);

    return (

        <>
            <h1>First Component</h1>
            <h1> Hello World!!! {count}</h1>
            <button onClick={() => setCount(1)}>Click Me!!!! </button>
        </>
    )


} 

export default MyComponent;