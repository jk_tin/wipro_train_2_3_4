function Movie({prop}) {
    console.log(prop)
  return (
    <>
      <div className="card" style={{ width: "200px" }} >
        <img
          src={prop.img}
          className="card-img-top"
          alt={prop.altmsg}
        />
        <div className="card-body">
          <h5 className="card-title">{prop.moviename}</h5>
          <p className="card-text">
           {prop.rating}
          </p>
          <a href="#" className="btn btn-primary">
            Watch Options
          </a>
        </div>
      </div>
    </>
  );
}
export default Movie;