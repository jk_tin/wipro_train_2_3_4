import logo from "./logo.svg";
import "./App.css";
import Movie from "./Movie"
import {useRef} from 'react';

function App() {
  // const [name,setName]=useState("Peter");
  const nameRef = useRef();
  const addressRef = useRef();
  const countryRef = useRef();
  function clickHandler()
  {
     console.log(nameRef.current.value);
     console.log(addressRef.current.value);
     console.log(countryRef.current.value);
  }
 
  return (
    <div className="App">
     

       {/* <Movie prop={{img:"https://m.media-amazon.com/images/M/MV5BYTY2ZjYyNGUtZGVkZS00MDNhLWIwMjMtZDk4MmQ5ZWI0NTY4XkEyXkFqcGdeQXVyMTY3MDE5MDY1._V1_QL75_UY207_CR13,0,140,207_.jpg"
        ,altmsg:"alt",
        rating:8.7,
        moviename:"The Boys",
        
        }} ></Movie>

    <Movie prop={{img:"https://m.media-amazon.com/images/M/MV5BM2QzMGVkNjUtN2Y4Yi00ODMwLTg3YzktYzUxYjJlNjFjNDY1XkEyXkFqcGc@._V1_QL75_UX140_CR0,0,140,207_.jpg"
        ,altmsg:"alt",
        rating:8.4,
        moviename:"House of the Dragon",
        
        }} ></Movie> */}
        <input type="name" name="name" ref={nameRef} required />
        <input type="address" name="address" ref={addressRef} required />
        <input type="country" name="country" ref={countryRef} required />

        <button onClick={clickHandler}>Click Me</button>

     </div>
  );
}

export default App;
