 
import './App.css';
import InputComponent from './InputComponent';
import TextComponent from './TextComponent';
import withHover from './withHover'
function App() {
  
  const TextComponentWithHover = withHover(TextComponent);
  const InputComponentWithHover = withHover(InputComponent);;

  return (
    <>
      <TextComponentWithHover text="This is HOC example"/> 
      <InputComponentWithHover type="text"/> 
    </>
  );
}

export default App;
