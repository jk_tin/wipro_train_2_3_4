import React from 'react';

const Button = ({ render, onClick }) => (
  <button onClick={onClick}>{render()}</button>
);

export default Button;