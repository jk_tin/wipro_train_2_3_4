import React, { useState,useEffect } from 'react';
import './App.css';

 

function App() {
 
   
  // const [formData, setFormData] = useState({
  //   id: '',
  //   description: '',
  // });
  const [description, setDescription] = useState('');
  const [items, setItems] = useState([]);
  const [flag, setFlag] = useState(false);
  useEffect(() => {
    fetch('http://localhost:8000/products')
       .then((response) => response.json())
       .then((data) => {
         // console.log(data);
          setItems(data);
          console.log(items);
       })
       .catch((err) => {
          console.log(err.message);
       });
 }, [description]);
 const handleChange = (e) => {
  setDescription(e.target.value);
  };
 
  const addItem = () => {
    console.log("add item"+description);
    // console.log(formData);
    if (description !== '') {
      console.log("here")
      const response =   fetch('http://localhost:8000/products', { // Replace with your endpoint URL
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }, // Specify JSON content type
        body: JSON.stringify({description:description}), // Convert form data to JSON
      })
      .then(response => response.json()) 
      .then(json => console.log(json));
       setDescription('');
       setFlag(true);
    }
  };

  

  const deleteItem = (id) => {
    console.log("deleted id="+id);
    const updatedItems = items.filter(item => item.id !== id);
    const response =   fetch('http://localhost:8000/products/'+id, { // Replace with your endpoint URL
      method: 'DELETE',
      // headers: { 'Content-Type': 'application/json' }  // Specify JSON content type
        // Convert form data to JSON
    })
    .then(response => response.json()) 
    .then(json => console.log(json));
     setDescription('');
     setFlag(true);
     setItems(updatedItems);
  };

  return (
    <div className="App">
      <h1>CRUD Example</h1>
      <div>
        <input
          type="text"
          value={description} onChange={handleChange}          
          placeholder="Enter item description"
        />
        <button onClick={addItem}>Add Item</button>
      </div>
      <ul>
        {items.map(item => (
          <li key={item.id}>
            {item.description}
            <button onClick={() => deleteItem(item.id)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default App;
