import React from 'react';
import Button from './Button';

const App = () => {
  const handleClick = () => {
    console.log('Button clicked!');
  };

  const handleClick1 = () => {
    console.log('Button clicked-1!');
  };

  return (
    <div>
      <Button
        render={() => <span>Click Me!</span>} // Render function defining button text
        onClick={handleClick} // Button click handler
      />
       <Button
        render={() => <span>Click Me-1!</span>} // Render function defining button text
        onClick={handleClick1} // Button click handler
      />
    </div>
  );
};

export default App;