 
import {withHover} from './Hoc';
import TextComponent from './TextComponent'
import InputComponentC from './InputComponentC';
export default function App()
{
    
    const InputComponentWithHover = withHover(InputComponentC);
    const TextComponentWithHover = withHover(TextComponent);
    return (
    
    <>
 
 <TextComponentWithHover
        text="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
        commodo consequat."
      />

      <InputComponentWithHover type="text" />
    </>
    );
}
