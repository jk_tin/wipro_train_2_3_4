const InputComponentC = ({ type, isHovered }) => {
    return (
      <input
        type={type}
        style={{ backgroundColor: isHovered ? "blue" : "white" }}
      />
    );
  };

export default InputComponentC;