import React from 'react'
import {Component} from 'react'


class App extends Component{

    name ="";
    constructor(props)
    {

        super(props);
        this.state = {favoritefood: "rice"};
        console.log("--Constructor--");
    }

    // static getDerivedStateFromProps(props, state) {
        
    //     return {favoritefood: props.favfod };
    //   }
    componentDidMount() {
        console.log("--componentDidMount--");
        setTimeout(() => {
          this.setState({favoritefood: "pizza"})
        }, 1000)
      }
      componentDidUpdate(prevProps, prevState) {
        if (prevState.favoritefood !== this.state.favoritefood) {
          console.log('favoritefood has been updated:', this.state.favoritefood);
        }
      }
      changeFood=()=>{

        this.setState({favoritefood: "Biriyani"})
      }

      shouldComponentUpdate(nextProps, nextState) {
        // Only re-render if the favoriteFood state has changed
        return true;
      }
      componentWillUpdate(nextProps, nextState) {

        console.log("--componentWillUpdate--");
      }
        
   render(){
    console.log("--render--");
    return <> 
        <h1>My Favorite Food is {this.state.favoritefood}</h1>
        <button type="button" onClick={this.changeFood}>Change food</button>
    </>

   }

}

export default App;