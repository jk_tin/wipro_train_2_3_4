import React from 'react'
import {Component} from 'react'

class Child extends Component{


    render(){
       
        return <> 
             
            <p> I am a Child </p>
        </>
    
       }


       componentWillUnmount() {
         console.log("I am going to be unmounted")
      }
    
    
};



export default Child;