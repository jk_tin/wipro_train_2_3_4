import React from "react";
import { Component } from "react";
import Child from "./Child";
import useState from "react";

class App extends Component {
  state = {
    showChild: true,
  };

  handleDelete = () => {
    this.setState({ showChild: false });
  };

  render() {
    const { showChild } = this.state;

    console.log("--render--");
    return (
      <>
        <div>
          {showChild && <Child />}
          <button type="button" onClick={this.handleDelete}>
            Delete Header
          </button>
        </div>
      </>
    );
  }
}

export default App;
