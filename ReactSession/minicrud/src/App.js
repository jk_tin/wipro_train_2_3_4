 
import './App.css';
import {useState} from 'react';
import {useEffect} from 'react';
function App() {

  const [description,setDescription]=useState("");
  const [items,setItems]=useState([]);
  const[flag,setFlag]=useState(false);
  useEffect(() => {
      const f=fetch("http://localhost:8000/products");
      f.then((response) => response.json())
      .then((data) => {
        // console.log(data);
         setItems(data);
         console.log(items);
      })
      .catch((err) => {
         console.log(err.message);
      });
  
  }, [flag]);
   


  function changeHandler(e)
  {
   console.log("--changeHandler--"+e.target.value);
   setDescription(e.target.value);

  }

  function addProducts()
  {
   setFlag(false);
    console.log("--addProducts--");
    fetch('http://localhost:8000/products', { // Replace with your endpoint URL
      method: 'POST',
      headers: { 'Content-Type': 'application/json' }, // Specify JSON content type
      body: JSON.stringify({description:description}), // Convert form data to JSON
    })
    .then(response => response.json()) 
    .then(json => {
      setFlag(true);
      setDescription("");
      console.log(json);
      
     }
   
   );

  }

  function deleteItem(id)
  {
   setFlag(false);
   fetch('http://localhost:8000/products/'+id, { // Replace with your endpoint URL
      method: 'DELETE',
      
    })
    .then(response => response.json()) 
    .then(json => {
      setFlag(true);
      //setDescription("");
      console.log(json);
   
     }
   
   );
  }

  return (
     <>
        <div>
        <input type="text" value={description} onChange={changeHandler}/>
        <button onClick={addProducts}>Add Products</button>
        </div>

        <ul>
        {items.map(item => (
          <li key={item.id}>
            {item.description}
            <button onClick={() => deleteItem(item.id)}>Delete</button>
          </li>
        ))}
      </ul>
     </>
     
  );
}

export default App;
