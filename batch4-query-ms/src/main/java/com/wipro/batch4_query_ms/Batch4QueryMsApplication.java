package com.wipro.batch4_query_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4QueryMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4QueryMsApplication.class, args);
	}

}
