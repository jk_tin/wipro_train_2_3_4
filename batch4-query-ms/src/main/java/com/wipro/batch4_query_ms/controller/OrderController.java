package com.wipro.batch4_query_ms.controller;
 

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4_query_ms.config.handler.QueryHandler;
import com.wipro.batch4_query_ms.entity.Order;

import java.util.List;

@RestController
public class OrderController {

	@Autowired
	QueryHandler queryHandler;
	
	@GetMapping("/order")
	List<Order> findAll()
	{
		return queryHandler.findAll();
		
	}
	@GetMapping("/order/{id}")
	Order findById(@PathVariable int id)
	{
		return queryHandler.findById(id);
		
	}
	
	
}
