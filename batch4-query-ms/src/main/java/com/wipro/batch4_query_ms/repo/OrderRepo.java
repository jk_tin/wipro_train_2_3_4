package com.wipro.batch4_query_ms.repo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4_query_ms.entity.*;
@Repository
public interface OrderRepo extends JpaRepository<Order,Integer>{

	Order findByOrderId(String orderId);
	
}
