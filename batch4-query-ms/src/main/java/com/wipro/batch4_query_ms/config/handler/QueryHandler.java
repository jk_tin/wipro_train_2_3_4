package com.wipro.batch4_query_ms.config.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch4_query_ms.repo.OrderRepo;
import java.util.List;
import com.wipro.batch4_query_ms.entity.*;
@Service
public class QueryHandler {
	@Autowired
	OrderRepo orderRepo;
	
	public List<Order> findAll()
	{
		return orderRepo.findAll();
	}
	
	public Order findById(int id)
	{
		return orderRepo.findById(id).get();
	}
	
}
