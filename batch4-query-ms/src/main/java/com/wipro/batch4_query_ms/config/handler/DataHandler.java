package com.wipro.batch4_query_ms.config.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wipro.batch4_query_ms.entity.Order;
import com.wipro.batch4_query_ms.repo.OrderRepo;
 
@Component
public class DataHandler {
	@Autowired
	OrderRepo orderRepo;

	@KafkaListener(topics = "order-msg", groupId = "batch4-group-id"
			,properties = {"spring.json.value.default.type=com.wipro.batch4_query_ms.entity.Order"})
	void readFromTopic(Order order)
	{
	  System.out.println("--Message Received--"+order);
	   
		Order orderDB= orderRepo.findByOrderId(order.getOrderId());
		if(null!=orderDB)
		{
			order.setId(orderDB.getId());	
			
		}
		orderRepo.save(order);
	}
	
	
}
