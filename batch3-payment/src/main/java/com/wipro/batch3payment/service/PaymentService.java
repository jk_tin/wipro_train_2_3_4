package com.wipro.batch3payment.service;

import com.wipro.batch3payment.entity.Payment;
import java.util.List;
public interface PaymentService {

	Payment save(Payment payment);
	List<Payment> findAll();
	Payment findById(int id);
	void deleteById(int id);
	Payment findByOrderId(int orderId);
}
