package com.wipro.batch3payment.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch3payment.entity.Payment;
import com.wipro.batch3payment.repo.PaymentRepo;
import com.wipro.batch3payment.service.PaymentService;

@Service
public class PaymentServiceImpl implements PaymentService {

	@Autowired
	PaymentRepo paymentRepo;
	
	@Override
	public Payment save(Payment payment) {
		 
		return paymentRepo.save(payment);
	}

	@Override
	public List<Payment> findAll() {
		// TODO Auto-generated method stub
		return paymentRepo.findAll();
	}

	@Override
	public Payment findById(int id) {
		// TODO Auto-generated method stub
		return paymentRepo.findById(id).get();
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub

	}

	@Override
	public Payment findByOrderId(int orderId) {
		// TODO Auto-generated method stub
		return paymentRepo.findByOrderId(orderId);
	}

}
