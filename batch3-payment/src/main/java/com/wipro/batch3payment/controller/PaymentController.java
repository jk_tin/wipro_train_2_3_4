package com.wipro.batch3payment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3payment.entity.Payment;
import com.wipro.batch3payment.service.PaymentService;

@RestController
public class PaymentController {
	@Autowired
	PaymentService paymentService;
	
	@PostMapping("/payment")
	Payment save(@RequestBody Payment payment)
	{
		return paymentService.save(payment);
		
	}
	@PutMapping("/payment")
	Payment update(@RequestBody Payment payment)
	{
		return paymentService.save(payment);
		
	}
	
	@GetMapping("/payment")
	List<Payment> findAll()
	{
		return paymentService.findAll();
	}
	
	@GetMapping("/payment/{id}")
	Payment findById(@PathVariable int id)
	{
		return paymentService.findById(id);
	}
	
	@DeleteMapping("/payment/{id}")
	void deleteById(@PathVariable int id)
	{
		  paymentService.deleteById(id);
	}
	@GetMapping("/paymentbyordid/{id}")
	Payment findByOrderId(@PathVariable int id)
	{
		return paymentService.findByOrderId(id);
	}

}
