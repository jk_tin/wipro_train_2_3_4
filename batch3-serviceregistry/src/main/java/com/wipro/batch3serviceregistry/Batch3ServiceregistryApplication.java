package com.wipro.batch3serviceregistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class Batch3ServiceregistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3ServiceregistryApplication.class, args);
	}

}
