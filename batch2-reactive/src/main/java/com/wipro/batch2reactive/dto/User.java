package com.wipro.batch2reactive.dto;


public class User {
	
	String name;
	String location;
	
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User(String name, String location) {
		super();
		this.name = name;
		this.location = location;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	@Override
	public String toString() {
		return "User [name=" + name + ", location=" + location + "]";
	}
	
	

}
