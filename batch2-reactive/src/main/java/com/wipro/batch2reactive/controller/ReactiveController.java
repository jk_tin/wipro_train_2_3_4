package com.wipro.batch2reactive.controller;

import java.util.List;
import java.time.Duration;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import com.wipro.batch2reactive.dto.*;
@RestController
public class ReactiveController {

	@GetMapping("/mono")
	Mono<User> getMono()
	{
		
		User user = new User("Jayanta","Kolkata");
		return Mono
			   .just(user)
			   .log();
		
	}
	@GetMapping("/flux")
	Flux<String> getFlux()
	{
		String [] list= {"Jayanta","Moumita","Ashok","Sai","Vasu"};
		return Flux
				.fromArray(list)
				.take(1)
				.delayElements(Duration.ofSeconds(3))				 
				.log();
	}
	
}
