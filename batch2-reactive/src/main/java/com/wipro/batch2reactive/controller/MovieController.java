package com.wipro.batch2reactive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch2reactive.dto.Movie;
import com.wipro.batch2reactive.service.MovieService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class MovieController {
	
	@Autowired
	MovieService movieService;
	
	@PostMapping("/movie")
	Mono<Movie> save(@RequestBody Movie m)
	{
		System.out.println(m);
		return movieService.save(m);
		
	}
	@GetMapping("/movie/{id}")
	Mono<Movie> save(@PathVariable int id)
	{
 
		return movieService.findById(id);
		
	}
	@PutMapping("/movie")
	void update(@RequestBody Movie m)
	{
 
		movieService.update(m);
		
	}
	
	@GetMapping("/movie")
	Flux<Movie> findAll()
	{
 
		return movieService.findAll();
		
	}
	
}
