package com.wipro.batch2reactive.repo;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch2reactive.dto.Movie;

@Repository
public interface MovieRepo extends R2dbcRepository<Movie, Integer> {

}
