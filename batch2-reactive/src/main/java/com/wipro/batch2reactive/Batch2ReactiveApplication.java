package com.wipro.batch2reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.r2dbc.repository.config.EnableR2dbcRepositories;

@SpringBootApplication
@EnableR2dbcRepositories
public class Batch2ReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2ReactiveApplication.class, args);
	}

}
