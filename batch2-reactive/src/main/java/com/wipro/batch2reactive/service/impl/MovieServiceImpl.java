package com.wipro.batch2reactive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

 
import com.wipro.batch2reactive.dto.Movie;
import com.wipro.batch2reactive.repo.MovieRepo;
import com.wipro.batch2reactive.service.MovieService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Service
public class MovieServiceImpl implements MovieService {

	@Autowired
	MovieRepo movieRepo;
	
	@Override
	public Mono<Movie>  save(Movie m) {
	
		return movieRepo.save(m);
	}

	
	 
	@Override
	public Mono<Movie> findById(int id) {
		// TODO Auto-generated method stub
		return movieRepo.findById(id);
	}

	@Override
	public Mono<Movie> update(Movie m) {
	 
		return movieRepo.save(m);
	}

	@Override
	public Flux<Movie> findAll() {
		// TODO Auto-generated method stub
		return movieRepo.findAll();
	}

}
