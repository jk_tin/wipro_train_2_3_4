package com.wipro.batch2reactive.service;

import com.wipro.batch2reactive.dto.Movie;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface MovieService {
	
	Mono<Movie> save(Movie m) ;
	Mono<Movie> findById(int id);
	Mono<Movie> update(Movie m);
	Flux<Movie> findAll();
}
