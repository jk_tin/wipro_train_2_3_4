package com.wipro.batch3reactive.service;

import com.wipro.batch3reactive.entity.Car;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CarService {
	
	Mono<Car>save(Car car);
	Flux<Car> findAll();
	Mono<Car> findById(int id);
	Mono<Car> update(Car car);
	Mono<Void> deleteById(int id);
	

}
