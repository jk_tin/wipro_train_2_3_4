package com.wipro.batch3reactive.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch3reactive.entity.Car;
import com.wipro.batch3reactive.repo.CarRepo;
import com.wipro.batch3reactive.service.CarService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
@Service
public class CarServiceImpl implements CarService {

	@Autowired
	CarRepo  carRepo;
	
	@Override
	public Mono<Car> save(Car car) {
		 
		return carRepo.save(car);
	}

	@Override
	public Flux<Car> findAll() {
		// TODO Auto-generated method stub
		return carRepo.findAll();
	}

	@Override
	public Mono<Car> findById(int id) {
		// TODO Auto-generated method stub
		return carRepo.findById(id);
	}

	@Override
	public Mono<Car> update(Car car) {
		// TODO Auto-generated method stub
		return carRepo.save(car);
	}

	@Override
	public Mono<Void> deleteById(int id) {
		// TODO Auto-generated method stub
		return carRepo.deleteById(id);
	}

}
