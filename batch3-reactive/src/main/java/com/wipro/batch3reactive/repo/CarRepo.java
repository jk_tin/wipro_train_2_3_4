package com.wipro.batch3reactive.repo;

import org.springframework.data.r2dbc.repository.R2dbcRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch3reactive.entity.Car;

@Repository
public interface CarRepo extends R2dbcRepository<Car, Integer> {

}
