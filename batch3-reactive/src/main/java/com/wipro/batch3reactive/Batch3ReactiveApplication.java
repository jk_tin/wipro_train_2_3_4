package com.wipro.batch3reactive;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3ReactiveApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3ReactiveApplication.class, args);
	}

}
