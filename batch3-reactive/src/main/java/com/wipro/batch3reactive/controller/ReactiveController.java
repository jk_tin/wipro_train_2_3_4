package com.wipro.batch3reactive.controller;

import java.time.Duration;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class ReactiveController {

	@GetMapping("/mono")
	Mono<String> getMono()
	{
		
		return Mono
		.just("Hello From Mono")
		.log()
		;
		
	}
	
	@GetMapping("/flux")
	Flux<String> getFlux()
	{
		
		String[] arr= {"Jayanta","Moumita","Amit","Vasu"};
		
		return Flux
		.fromArray(arr)
		.take(1)
		.delayElements(Duration.ofSeconds(3))
		.repeat()
		.log();
		
		
	}
	
	
}
