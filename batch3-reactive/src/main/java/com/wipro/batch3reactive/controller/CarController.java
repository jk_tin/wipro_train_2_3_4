package com.wipro.batch3reactive.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3reactive.entity.Car;
import com.wipro.batch3reactive.service.CarService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CarController {
	@Autowired
	CarService carService;
	
	@PostMapping("/car")
	Mono<Car> save(@RequestBody Car car)
	{
		
		return carService.save(car);
	}
	
	@GetMapping("/car/{id}")
	Mono<Car> findById(@PathVariable int id)
	{
		
		return carService.findById(id);
	}
	
	@GetMapping("/car")
	Flux<Car> findAll()
	{
		
		return carService.findAll();
	}
	@DeleteMapping("/car/{id}")
	Mono<Void> deleteById(@PathVariable int id)
	{
		
		return carService.deleteById(id);
	}
	
	@PutMapping("/car")
	Mono<Car> update(@RequestBody Car car)
	{
		
		return carService.update(car);
	}
	

}
