package com.wipro.batch3order.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3order.dto.OrderVO;
import com.wipro.batch3order.entity.*;
import com.wipro.batch3order.service.OrderService;
import com.wipro.batch3order.dto.Payment;

@RestController
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	
	@PostMapping("/order")
	void save(@RequestBody OrderVO orderVO)
	{
		System.out.println(orderVO);
		orderService.save(orderVO);
		
	}
	
	
	@GetMapping("/order/{id}")
	OrderVO findById(@PathVariable int id )
	{
		return orderService.findById(id);
	}

}
