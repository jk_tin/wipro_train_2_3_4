package com.wipro.batch3order.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch3order.entity.*;

@Repository
public interface OrderRepo extends JpaRepository<Order, Integer> {

}
