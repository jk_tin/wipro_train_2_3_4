package com.wipro.batch3order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class Batch3OrderApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3OrderApplication.class, args);
	}

}
