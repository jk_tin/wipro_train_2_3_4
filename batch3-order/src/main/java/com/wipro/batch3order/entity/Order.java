package com.wipro.batch3order.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="order_details")
public class Order {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column(name="order_description")
	String ordeDescr;
	
	@Column(name="is_processed")
    boolean isProcessed;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getOrdeDescr() {
		return ordeDescr;
	}

	public void setOrdeDescr(String ordeDescr) {
		this.ordeDescr = ordeDescr;
	}

	public boolean isProcessed() {
		return isProcessed;
	}

	public void setProcessed(boolean isProcessed) {
		this.isProcessed = isProcessed;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", ordeDescr=" + ordeDescr + ", isProcessed=" + isProcessed + "]";
	}
    
    
	

}
