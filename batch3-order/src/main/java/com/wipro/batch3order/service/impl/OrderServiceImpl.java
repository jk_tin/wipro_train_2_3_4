package com.wipro.batch3order.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.wipro.batch3order.dto.OrderVO;
import com.wipro.batch3order.dto.Payment;
import com.wipro.batch3order.entity.Order;
import com.wipro.batch3order.repo.OrderRepo;
import com.wipro.batch3order.service.OrderService;

@Service
public class OrderServiceImpl  implements OrderService{
    @Autowired
	OrderRepo orderRepo;

    @Autowired
    RestTemplate restTemplate;
    
    
	@Override
	public void save(OrderVO orderVO) {
		 
		Order order=orderVO.getOrder();
		System.out.println("id---before save="+order.getId());
		order=orderRepo.save(order);
		System.out.println("id---after save="+order.getId());
		
		//String url="http://localhost:9091/payment";
		String url="http://BATCH3-PAYMENT-MS/payment";
		Payment pmnt= orderVO.getPayment();
		pmnt.setOrderId(order.getId());
		//RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<Payment> response=restTemplate.postForEntity(url, pmnt,Payment.class);
		if(response.getStatusCode().value()==200)
		{
			order.setProcessed(true);
			orderRepo.save(order);
		}
		
		
	}




	@Override
	public OrderVO findById(int id) {
		
		Order order=orderRepo.findById(id).get();
		OrderVO orderVO= new OrderVO();
		Payment pmnt=null;
		if(null!=order)
		{
			//RestTemplate restTemplate = new RestTemplate();
			//String url="http://localhost:9091/paymentbyordid/"+order.getId();
			String url="http://BATCH3-PAYMENT-MS/paymentbyordid/"+order.getId();
			
			ResponseEntity<Payment> response=restTemplate.getForEntity(url, Payment.class);
			System.out.println(response.getStatusCode().value()); 
			if(response.getStatusCode().value()==200)
			{
				
				pmnt= response.getBody();
				System.out.println("--here-"+pmnt);
				
			}
			orderVO.setOrder(order);
			orderVO.setPayment(pmnt);
		}
		
		
		
		return orderVO;
	}
	
	 

}
