package com.wipro.batch3order.service;

import com.wipro.batch3order.dto.OrderVO;

public interface OrderService {
	
	void save(OrderVO orderVO);
	
	OrderVO findById(int id);

}
