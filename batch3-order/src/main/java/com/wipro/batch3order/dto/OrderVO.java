package com.wipro.batch3order.dto;

import com.wipro.batch3order.entity.Order;

public class OrderVO {
	
	Order order;
	Payment payment;
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	public Payment getPayment() {
		return payment;
	}
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	@Override
	public String toString() {
		return "OrderVO [order=" + order + ", payment=" + payment + "]";
	}
	

}
