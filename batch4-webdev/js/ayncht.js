async function asyncFunction() {
    try {
      console.log("Start");
      const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve("Resolved");
        }, 2000);
      });
      const result = await promise;
      console.log(result);
      console.log("End");
    } catch (error) {
      console.error(error);
    }
  }


  console.log("--Going to call asyncFunction---");
  asyncFunction();
  console.log("Function is called")
