package com.wipro.angularms;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.angularms.entity.Employee;
import com.wipro.angularms.service.EmployeeService;

@RestController
public class EmployeeController {
	
	@Autowired
	EmployeeService employeeService;
	@GetMapping("/emp")
	List<Employee> findAll()
	{
		return employeeService.findAll();
	}
	 
}
