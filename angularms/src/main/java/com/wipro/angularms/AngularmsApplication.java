package com.wipro.angularms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AngularmsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AngularmsApplication.class, args);
	}

}
