package com.wipro.angularms.service;
import com.wipro.angularms.entity.Employee;
import java.util.List;
public interface EmployeeService {

	List<Employee> findAll();
}
