package com.wipro.angularms.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.wipro.angularms.entity.Employee;
import com.wipro.angularms.repo.EmployeeRepo;
import com.wipro.angularms.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	
	EmployeeRepo employeeRepo;

	public List<Employee> findAll() {
		// TODO Auto-generated method stub
		return employeeRepo.findAll();
	}

}
