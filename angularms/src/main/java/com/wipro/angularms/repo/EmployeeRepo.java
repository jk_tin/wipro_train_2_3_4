package com.wipro.angularms.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.angularms.entity.Employee;

 

@Repository
public interface EmployeeRepo extends JpaRepository<Employee, Integer> {

}
