package com.wipro.batch2apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient
@SpringBootApplication
public class Batch2ApigatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2ApigatewayApplication.class, args);
	}

}
