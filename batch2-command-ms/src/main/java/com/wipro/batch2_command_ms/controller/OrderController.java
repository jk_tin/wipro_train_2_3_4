package com.wipro.batch2_command_ms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.yaml.snakeyaml.internal.Logger;

import com.wipro.batch2_command_ms.entity.Order;
import com.wipro.batch2_command_ms.entity.handler.CommandHandler;

@RestController
public class OrderController {

	@Autowired
	CommandHandler commandHandler;
 
	
	@PostMapping("/order")
	void save(@RequestBody Order order)
	{
		 
		commandHandler.handle(order);
	}
}
