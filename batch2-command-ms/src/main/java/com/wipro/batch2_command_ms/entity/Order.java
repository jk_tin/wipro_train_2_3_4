package com.wipro.batch2_command_ms.entity;

import java.time.LocalDateTime;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.PrePersist;
import jakarta.persistence.Table;

@Entity
@Table(name="order_details")
public class Order {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	@Column(name="order_desc")
	String orderDesc;
	
	@Column(name="order_id")
	String orderId;
	
	@Column(name="order_status")
	String status;
	
	@Column(name="order_time")
	LocalDateTime orderDateTime;
	
	@PrePersist
	void setDate()
	{
		this.orderDateTime= LocalDateTime.now();
		
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getOrderDesc() {
		return orderDesc;
	}
	public void setOrderDesc(String orderDesc) {
		this.orderDesc = orderDesc;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", orderDesc=" + orderDesc + ", orderId=" + orderId + ", status=" + status
				+ ", orderDateTime=" + orderDateTime + "]";
	}
	 
	
	

}
