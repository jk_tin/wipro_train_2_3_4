package com.wipro.batch2_command_ms.entity.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch2_command_ms.entity.Order;
import com.wipro.batch2_command_ms.producer.OrderProducer;
import com.wipro.batch2_command_ms.repo.OrderRepo;

@Service
public class CommandHandler {
	@Autowired
	OrderRepo orderRepo;
	@Autowired
	OrderProducer orderProducer;
	
	public void handle(Order order)
	{
		
		Order orderNew=new Order();
		orderNew.setOrderDesc(order.getOrderDesc());
		orderNew.setOrderId(order.getOrderId());
		orderNew.setStatus(order.getStatus());
		orderRepo.save(orderNew);
		
		orderProducer.sendOrderToBroker(orderNew);
		
	}
	

}
