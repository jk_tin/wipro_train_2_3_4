package com.wipro.batch2_command_ms.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.wipro.batch2_command_ms.entity.Order;
@Component
public class OrderProducer {
	@Autowired
	KafkaTemplate<String,Order> kafkaTemplate;
	
	public void sendOrderToBroker(Order order)
	{
		kafkaTemplate.send("order-topic",order);
	}

}
