package com.wipro.batch2_command_ms.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch2_command_ms.entity.Order;
@Repository
public interface OrderRepo extends JpaRepository<Order,Integer> {

}
