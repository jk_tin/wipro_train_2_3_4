package com.wipro.batch2_command_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2CommandMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2CommandMsApplication.class, args);
	}

}
