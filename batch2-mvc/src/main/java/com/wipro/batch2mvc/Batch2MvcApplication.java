package com.wipro.batch2mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2MvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2MvcApplication.class, args);
	}

}
