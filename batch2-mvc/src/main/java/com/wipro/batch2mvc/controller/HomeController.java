package com.wipro.batch2mvc.controller;

import java.util.List;
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.wipro.batch2mvc.dto.*;
@Controller
public class HomeController {
	
	@GetMapping("/")
	String welcome(Model m)
	{
		 
		return "index";
	}
	
	@GetMapping("/register")
	String register(Model m)
	{
		System.out.println("--reached here--");
		User user=new User();
		m.addAttribute("user",user);	
		List<String> list= new ArrayList();
		list.add("Doctor");
		list.add("Engineer");
		list.add("Accountant");
		list.add("Musician");
		m.addAttribute("listProfession",list);			
		return "register_proc";
	}
	
	@PostMapping("/register_done")
	String registerDone(@ModelAttribute("user") User user)
	{
	    System.out.println("---Here---");
		return "register_done";
	}

}
