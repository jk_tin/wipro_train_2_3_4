package com.wipro.batch4jpacardinality.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4jpacardinality.repo.CartRepo;
import com.wipro.batch4jpacardinality.repo.CourseRepo;
import com.wipro.batch4jpacardinality.repo.PersonRepo;
import com.wipro.batch4jpacardinality.repo.StudentRepo;
import com.wipro.batch4jpacardinality.entity.*;
import java.util.Set;
import java.util.HashSet;

@RestController
public class PersonController {
	
	@Autowired
	PersonRepo personRepo;
	@Autowired
	CartRepo cartRepo;
	
	@Autowired
	StudentRepo studentRepo;
	
	@Autowired
	CourseRepo courseRepo;
	
	@GetMapping("/saveperson")
	void savePerson()
	{
		Pan p = new Pan();
		p.setPanNumber("AEGHD7801F");
		
		Person person= new Person();
		person.setName("Amit");
		person.setPan(p);
		personRepo.save(person);
		
		
	}
	
	@GetMapping("/savecart")
	void saveCart()
	{
		 Cart cart= new Cart();
		 Set<Item> items= new HashSet();
		 
		 Item item1= new Item();
		 item1.setDescription("item1");
		 item1.setCart(cart);
		 Item item2= new Item();
		 item2.setDescription("item2");
		 item2.setCart(cart);
		 
		 Item item3= new Item();
		 item3.setDescription("item3");
		 item3.setCart(cart);
		 items.add(item1);
		 items.add(item2);
		 items.add(item3);		 
		 cart.setItems(items);
		 cartRepo.save(cart);
		 
		 
		 
		
	}

	@GetMapping("/savestudent")
	void saveStudent()
	{
	  Student s1= new Student();
	  s1.setName("Swami");
	  Student s2= new Student();
	  s2.setName("Vasu");
	  
	  Student s3= new Student();
	  s3.setName("Amit");
	  
	  Set<Student> studentList= new HashSet();
	  studentList.add(s1);
	  studentList.add(s2);
	  studentList.add(s3);
	  
	  
	  Course course1=new Course();
	  course1.setCourseName("Physics");
	  course1.setStudentList(studentList);
	  Course course2=new Course();
	  course2.setCourseName("Maths");
	  course2.setStudentList(studentList);	  
	  Course course3=new Course();
	  course3.setCourseName("Chemsitry");
	  course3.setStudentList(studentList);
	  
	  Set<Course> courseList= new HashSet();
	  courseList.add(course1);
	  courseList.add(course2);
	  courseList.add(course3);
	  s1.setCourse(courseList);
	  
	 // courseRepo.save(course3);
	  studentRepo.save(s1);
	  
	}

}
