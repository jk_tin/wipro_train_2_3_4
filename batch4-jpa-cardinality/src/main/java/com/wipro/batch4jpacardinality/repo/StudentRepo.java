package com.wipro.batch4jpacardinality.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import com.wipro.batch4jpacardinality.entity.*;
public interface StudentRepo extends JpaRepository<Student, Integer> {

}
