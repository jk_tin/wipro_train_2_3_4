package com.wipro.batch4jpacardinality.repo;
import com.wipro.batch4jpacardinality.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CourseRepo extends JpaRepository<Course, Integer> {

}
