package com.wipro.batch4jpacardinality.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4jpacardinality.entity.Person;
@Repository
public interface PersonRepo extends JpaRepository<Person, Integer> {

}
