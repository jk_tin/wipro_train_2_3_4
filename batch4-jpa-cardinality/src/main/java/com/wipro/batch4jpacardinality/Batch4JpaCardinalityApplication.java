package com.wipro.batch4jpacardinality;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4JpaCardinalityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4JpaCardinalityApplication.class, args);
	}

}
