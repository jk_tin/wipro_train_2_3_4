package com.wipro.batch4jpacardinality.entity;

import java.util.Set;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinTable;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.Table;
import jakarta.persistence.JoinColumn;

@Entity
@Table(name="student")
public class Student {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@Column
	String name;
	
	
	
	@ManyToMany (fetch = FetchType.LAZY,
		      cascade =     CascadeType.ALL)		           
	  @JoinTable(name = "student_course",
	  joinColumns = @JoinColumn(name = "student_id"), 
	 inverseJoinColumns = 
	 @JoinColumn(name = "course_id"))
	Set<Course> course;



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public Set<Course> getCourse() {
		return course;
	}



	public void setCourse(Set<Course> course) {
		this.course = course;
	}



	 
	
	
	
	
	
	
}
