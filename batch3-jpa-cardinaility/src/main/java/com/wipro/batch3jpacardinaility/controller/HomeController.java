package com.wipro.batch3jpacardinaility.controller;

import java.util.Set;
import java.util.HashSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch3jpacardinaility.repo.CartRepo;
import com.wipro.batch3jpacardinaility.repo.PersonRepo;
import com.wipro.batch3jpacardinaility.repo.StudentRepo;
import com.wipro.batch3jpacardinaility.entity.*;
@RestController
public class HomeController {
	
	@Autowired
	PersonRepo personRepo;
	@Autowired
	CartRepo cartRepo;	 
	@Autowired
	StudentRepo studentRepo;
	
	@GetMapping("/person")
	void savePerson()
	{
		Person person= new Person();
		person.setName("Jayaneta");
		Pan pan = new Pan();
		pan.setPanNumber("AERTG5607D");
		person.setPan(pan);
		personRepo.save(person);
		
	}
	
	@GetMapping("/cart")
	void saveCart()
	{
		Cart cart=new Cart();
		Item item1= new Item();
		item1.setItemDescription("Item-01");
		Item item11= new Item();
		item11.setItemDescription("Item-02");
		Item item111= new Item();
		item111.setItemDescription("Item-03");
		item1.setCart(cart);
		item11.setCart(cart);
		item111.setCart(cart);
		Set<Item> items= new HashSet();
		items.add(item1);	 
		items.add(item11);
		items.add(item111);		
		cart.setCartName("CART-01");
		cart.setItems(items);		
		cartRepo.save(cart);
		
	}
	@GetMapping("/student")
	void saveStudent() {
		
		Student student1 = new Student();
		student1.setStudentName("Moumita");
		Student student11 = new Student();
		student11.setStudentName("Amit");
		Student student111 = new Student();
		student111.setStudentName("Swami");
		
		Set<Student> studentList= new HashSet();
		studentList.add(student1);
		studentList.add(student11);
		studentList.add(student111);
		
		
		Course course1= new Course();
		course1.setCourseName("Physics");		
		Course course11= new Course();
		course11.setCourseName("Chemistry");
		Course course111= new Course();
		course111.setCourseName("Mathematics");
		Set<Course> courseList= new HashSet();
		course1.setStudents(studentList);
		course11.setStudents(studentList);
		course111.setStudents(studentList);
		courseList.add(course1);
		courseList.add(course11);
		courseList.add(course111);
		
		student1.setCourse(courseList);
		student11.setCourse(courseList);
		student111.setCourse(courseList);
		
		studentRepo.save(student1);
		
		
	}
}
