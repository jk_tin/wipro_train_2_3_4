package com.wipro.batch3jpacardinaility;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3JpaCardinailityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3JpaCardinailityApplication.class, args);
	}

}
