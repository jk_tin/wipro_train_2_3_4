package com.wipro.batch3jpacardinaility.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wipro.batch3jpacardinaility.entity.Cart;

public interface CartRepo extends JpaRepository<Cart, Integer> {

}
