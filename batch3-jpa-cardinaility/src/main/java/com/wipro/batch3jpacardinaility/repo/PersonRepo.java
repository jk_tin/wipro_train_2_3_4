package com.wipro.batch3jpacardinaility.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch3jpacardinaility.entity.Person;
@Repository
public interface PersonRepo extends JpaRepository<Person, Integer> {

}
