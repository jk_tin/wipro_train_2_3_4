package com.wipro.batch3jpacardinaility.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch3jpacardinaility.entity.Student;
@Repository
public interface StudentRepo extends JpaRepository<Student,Integer> {
	
}
