package com.wipro.batch4_kafka_demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4KafkaDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4KafkaDemoApplication.class, args);
	}

}
