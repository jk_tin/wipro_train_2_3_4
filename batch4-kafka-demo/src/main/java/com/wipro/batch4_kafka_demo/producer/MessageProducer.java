package com.wipro.batch4_kafka_demo.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class MessageProducer {
	
	
	@Autowired
	KafkaTemplate kafkaTemplate;
	
	
	public void publishMessage(String topicName,String message)
	{
		kafkaTemplate.send(topicName, message);
		
	}

}
