package com.wipro.batch4_kafka_demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4_kafka_demo.dto.Message;
import com.wipro.batch4_kafka_demo.producer.MessageProducer;

@RestController
public class MessageController {
	
	@Autowired
	MessageProducer messageProducer;
	
	@PostMapping("/msg")
	void sendMEssage(@RequestBody Message message)
	{
		messageProducer.publishMessage(message.getTopic(), message.getMessage());
		
		
	}

}
