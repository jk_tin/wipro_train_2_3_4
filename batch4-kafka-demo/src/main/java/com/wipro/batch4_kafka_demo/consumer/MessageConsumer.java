package com.wipro.batch4_kafka_demo.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
@Component
public class MessageConsumer {
	
	@KafkaListener(topics = "election-result", groupId = "batch4-group-id")
	void readFromTopic(String message)
	{
		
		System.out.println(message);
	}

}
