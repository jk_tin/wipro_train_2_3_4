package com.wipro.batch4_zpikin_ms_1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class Batch4ZpikinMs1Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch4ZpikinMs1Application.class, args);
	}

}
