package com.wipro.batch4_zpikin_ms_1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import com.wipro.batch4_zpikin_ms_1.integration.*;
import org.springframework.http.*;

@RestController
public class HomeController {
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MS2Integration ms2Integration;
	
	Logger logger= LoggerFactory.getLogger(HomeController.class);
	@GetMapping("/hello")
	String hello()
	{
		logger.info("--Logging From MS1--");
		String url="http://localhost:9011/greet";
		String response= restTemplate.getForObject(url,String.class);
		return response;
		
	}
	
	@GetMapping("/hellov2")
	String helloV2()
	{
		ResponseEntity<String> response= ms2Integration.getHelloV2();
		return response.getBody();
	}

}
