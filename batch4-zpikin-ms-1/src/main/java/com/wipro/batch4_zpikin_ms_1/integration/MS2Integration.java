package com.wipro.batch4_zpikin_ms_1.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(name = "Ms2Integration",
url = "http://localhost:9011/")
public interface MS2Integration {
	@GetMapping("/greet2")
	ResponseEntity<String> getHelloV2();

}
