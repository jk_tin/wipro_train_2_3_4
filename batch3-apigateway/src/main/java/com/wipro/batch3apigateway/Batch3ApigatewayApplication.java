package com.wipro.batch3apigateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
@EnableDiscoveryClient
@SpringBootApplication
public class Batch3ApigatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3ApigatewayApplication.class, args);
	}

}
