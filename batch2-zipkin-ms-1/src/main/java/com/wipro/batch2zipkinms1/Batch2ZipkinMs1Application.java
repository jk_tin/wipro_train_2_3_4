package com.wipro.batch2zipkinms1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch2ZipkinMs1Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch2ZipkinMs1Application.class, args);
	}

}
