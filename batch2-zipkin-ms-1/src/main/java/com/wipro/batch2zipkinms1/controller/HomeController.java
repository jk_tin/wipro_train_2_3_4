package com.wipro.batch2zipkinms1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
public class HomeController {
	
 Logger logger= LoggerFactory.getLogger(HomeController.class);

  @Autowired
  RestTemplate restTemplate;
    @CrossOrigin("*")
	@GetMapping("/greet")
	public String greet()
	{
		String url="http://localhost:9002/greet2";
		String output=restTemplate.getForObject(url, String.class);
		logger.info("--Inside- Greet--"+output);
		return output;
	}
	
	@GetMapping("/greetnew")
	public String greetnew()
	{
		String url="http://localhost:9002/greetnew";
		String output=restTemplate.getForObject(url, String.class);
		logger.info("--Inside- Greet--"+output);
		return output;
	}
	
	

}
