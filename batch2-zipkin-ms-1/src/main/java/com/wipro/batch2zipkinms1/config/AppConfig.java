package com.wipro.batch2zipkinms1.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class AppConfig {
	
	@Bean
	RestTemplate getResTemplate(RestTemplateBuilder restTemplateBuilder)
	{
		
		return restTemplateBuilder
				.build();
	}

}
