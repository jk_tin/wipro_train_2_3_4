import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
3


@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'second-app';
  name="Jayanta";
  btndisabled=false;
  color="green";
  fontSize="10px";
  value1="Start";

  text="";
  btnClick()
  {
    this.name=this.name+" Das";
    this.btndisabled=true;
    this.color="red";
    this.fontSize="20px";
    this.value1="New Text";
  }


  onKeyUp(x:any)
  {

    this.text=x.target.value;

  }

  checkValue()
  {

    console.log(this.value1);
  }
}
