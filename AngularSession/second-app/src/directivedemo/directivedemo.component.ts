import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-directivedemo',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './directivedemo.component.html',
  styleUrl: './directivedemo.component.css'
})
export class DirectivedemoComponent {
  isLoggedIn=false;
  username="Jayanta";
  items:string[]=['TV','MOBILE','LAPTOP'];
  selectedValue='one';
  num='2';
  btnClick()
  {
    this.isLoggedIn=true;

  }

}
