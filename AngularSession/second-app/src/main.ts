import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { DirectivedemoComponent } from './directivedemo/directivedemo.component';
import { AttributedirectiveComponent } from './attributedirective/attributedirective.component';

bootstrapApplication(AttributedirectiveComponent, appConfig)
  .catch((err) => console.error(err));
