interface  Movie
{
    movieName:string;
    movieRating:number;
    genre:string;

}



function printMovieName(movie:Movie)
{
    console.log(movie.movieName);
    console.log(movie.movieRating);
    console.log(movie.genre);
}


let m :Movie={
    movieName:"Alice in wonderland",
    movieRating:8.1,
    genre:"Fantasy",
 
    
}

printMovieName(m);


function getRandomElement<T>(items: T[]): T {
    let randomIndex = Math.floor(Math.random() * items.length);
    return items[randomIndex];
}

let arr:Number[]=[1,45,6,98];

let v=getRandomElement<Number>(arr);
