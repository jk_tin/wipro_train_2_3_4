class Person{

    fname:string;
    lname:string;
    constructor(fname:string,lname:string){
       this.fname=fname;
       this.lname=lname;
    }


    getFullName():string{

        return this.fname+" "+ this.lname;
    }


}


let p:Person = new Person("Jayanta","Das");

console.log(p.getFullName());


class Emp extends Person{


}
