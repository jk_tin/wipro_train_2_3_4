import { Component,DoCheck } from '@angular/core';

@Component({
  selector: 'app-parent1-child-2',
  standalone: true,
  imports: [],
  templateUrl: './parent1-child-2.component.html',
  styleUrl: './parent1-child-2.component.css'
})
export class Parent1Child2Component implements DoCheck {
  ngDoCheck(): void {
    console.log("--Parent1-Child2-Component-Change-")
}
}
