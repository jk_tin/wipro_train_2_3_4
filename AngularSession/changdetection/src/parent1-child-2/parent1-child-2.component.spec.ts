import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Parent1Child2Component } from './parent1-child-2.component';

describe('Parent1Child2Component', () => {
  let component: Parent1Child2Component;
  let fixture: ComponentFixture<Parent1Child2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Parent1Child2Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Parent1Child2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
