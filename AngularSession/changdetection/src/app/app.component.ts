import { Component, DoCheck } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {Parent1Component}  from '../parent1/parent1.component';
import { Parent1Child1Component } from '../parent1-child-1/parent1-child-1.component';
import { Parent1Child2Component } from '../parent1-child-2/parent1-child-2.component';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [Parent1Child1Component,Parent1Child2Component,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements DoCheck{
  title = 'changdetection';
  counter=2;
  myStr:any[]=[];
  msgFromParent:any[]=[];
  change()
  {
    this.myStr.push("Jayanta");
    this.msgFromParent.push("Message from Parent");
    this.counter++;

  }

  //ngZone
  ngDoCheck(): void {
      console.log("--changing--")
  }
}
