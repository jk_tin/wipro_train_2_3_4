
import { Component, DoCheck } from '@angular/core';
import { Parent1Child1Component } from '../parent1-child-1/parent1-child-1.component';
import { Parent1Child2Component } from '../parent1-child-2/parent1-child-2.component';
@Component({
  selector: 'app-parent1',
  standalone: true,
  imports: [Parent1Child1Component,Parent1Child2Component],
  templateUrl: './parent1.component.html',
  styleUrl: './parent1.component.css'
})
export class Parent1Component implements DoCheck{

  ngDoCheck(): void {
    console.log("--Parent1Component-Change-")
}

parentClick()
{

  console.log("--Parent1 Clicked-")
}
}
