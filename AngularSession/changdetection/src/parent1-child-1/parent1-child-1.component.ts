import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component ,DoCheck, Input} from '@angular/core';

@Component({
  selector: 'app-parent1-child-1',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './parent1-child-1.component.html',
  styleUrl: './parent1-child-1.component.css',
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class Parent1Child1Component implements DoCheck {
  @Input()
  myStr:any[]=[];

  ngDoCheck(): void {
    console.log("--Parent1-Child1-Component-Change-")
}

onClick()
{

  this.myStr.push("Parent 1 Child 1")
}


}
