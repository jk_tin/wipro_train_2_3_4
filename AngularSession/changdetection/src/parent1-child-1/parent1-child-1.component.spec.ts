import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Parent1Child1Component } from './parent1-child-1.component';

describe('Parent1Child1Component', () => {
  let component: Parent1Child1Component;
  let fixture: ComponentFixture<Parent1Child1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [Parent1Child1Component]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(Parent1Child1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
