import { Component, Input, OnInit,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-student',
  standalone: true,
  imports: [],
  templateUrl: './student.component.html',
  styleUrl: './student.component.css'
})
export class StudentComponent implements OnInit {

  @Input() myinputMsg: string="";
  @Output() myOutput: EventEmitter<string> = new EventEmitter();
  outputMessage: string = "I am child component.";
  ngOnInit() {
    console.log(this.myinputMsg);
  }

  sendValues() {
    this.myOutput.emit(this.outputMessage);

  }
}
