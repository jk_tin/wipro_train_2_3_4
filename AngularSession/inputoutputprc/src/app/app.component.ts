import { RouterOutlet } from '@angular/router';
import { Component, Input, OnInit } from '@angular/core';
import { StudentComponent } from '../student/student.component';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,StudentComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  @Input() myInputMessage: string="I am a parent";
  title = 'inputoutputprc';

  ngOnInit() {
    console.log(this.myInputMessage);
  }

  GetChildData(data:any){
    console.log(data);
    this.title=data;
 }
}
