import { Component, Input, OnChanges, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit , OnChanges{
  title = 'customer-app';

  @Input()
  myInput='';


  ngOnChanges()
  {
    console.log("--ngOnChanges--");

  }


  ngOnInit(): void {
    console.log("--ngOnInit--")
  }




}
