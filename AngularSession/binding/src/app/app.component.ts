import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'binding';
  name = 'Peter';
  buttonDisabled=false;
  color="red";
  value="JKD"
  updateName() {
    this.name = 'John';
    this.buttonDisabled = true;
    this.color="green";
  }


}
