import { Injectable } from '@angular/core';
import { IUSer } from '../IUser';
import { Observable, catchError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class FormServiceService {

  constructor(private http: HttpClient) { }

  // saveData(user:IUSer)
  // {
  //   console.log("user data--"+user.email)
  // }

  saveData(data: IUSer): Observable<IUSer> {
    const headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this.http.post<any>(`http://localhost:3000/users`, data, { headers })

}
}
