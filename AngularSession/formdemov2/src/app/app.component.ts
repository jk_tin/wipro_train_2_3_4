import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import   { IUSer } from '../IUser';
import { FormServiceService } from '../service/form-service.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'formdemov2';
  private user:IUSer={name:"",email:""};

  constructor(private formService:FormServiceService){}

  onSubmit(form: NgForm) {
    if (form.valid) {
      // Process the form data here
      console.log(form.value);
      this.user=form.value;
      // user1:IUSer={name:"",email:""};
      this.formService.saveData(this.user).subscribe({
        next: (response) => {
          console.log('POST request successful:', response);
          // Handle response as needed
        },
        error: (error) => {
          console.error('Error occurred during POST request:', error);
          // Handle error as needed
        }
      });


    }
  }

}
