import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { country } from './country';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FormsModule,CommonModule ],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})

export class AppComponent {
  title = 'formdemo';

  countryList:country[] = [
    new country("1", "India"),
    new country('2', 'UAE'),
    new country('3', 'USA')
  ];


   onSubmit(singUpForm:any) {
      console.log(singUpForm.value);

    }
}
