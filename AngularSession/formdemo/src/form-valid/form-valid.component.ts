import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';

@Component({
  selector: 'app-form-valid',
  standalone: true,
  imports: [FormsModule,CommonModule],
  templateUrl: './form-valid.component.html',
  styleUrl: './form-valid.component.css'
})
export default class FormValidComponent {

  onSubmit(form: NgForm) {
    if (form.valid) {
      // Process the form data here
      console.log(form.value);
    }
  }

}
