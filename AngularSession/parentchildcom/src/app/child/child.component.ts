import { Component,Input, OnInit,Output,EventEmitter} from '@angular/core';

@Component({
  selector: 'app-child',
  standalone: true,
  imports: [],
  templateUrl: './child.component.html',
  styleUrl: './child.component.css'
})
export class ChildComponent implements OnInit {
  @Input() myinput:string='';
  @Output() myOutput: EventEmitter<string> = new EventEmitter();

  outmessage:string="message from Child to Parent";

  message:string='';
  ngOnInit()
  {
    console.log("--received from parent--" +this.myinput);
    this.message=this.myinput;
  }

  sendMessage()
  {
    this.myOutput.emit(this.outmessage);

  }
}
