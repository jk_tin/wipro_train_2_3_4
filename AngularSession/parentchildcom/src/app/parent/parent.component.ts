import { Component,   OnInit } from '@angular/core';
import { ChildComponent } from '../child/child.component';

@Component({
  selector: 'app-parent',
  standalone: true,
  imports: [ChildComponent],
  templateUrl: './parent.component.html',
  styleUrl: './parent.component.css'
})
export class ParentComponent implements OnInit{

  title:string="Parent Componet";
  inputmessage:string="This  is a message from Parent To Child";

  ngOnInit()
  {


  }

  receiveData(data:string)
  {
    console.log(data);
    this.title=data;
  }

}
