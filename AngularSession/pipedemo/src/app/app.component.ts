import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SecondComponent } from '../second/second.component';
import { CommonModule } from '@angular/common';
import {DefaultPipe} from './default.pipe'
import { HttpClientModule } from '@angular/common/http';
import { ExternalService } from '../external.service';
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [ DefaultPipe,CommonModule,HttpClientModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'pipedemo';
  currentDate: Date=new Date();
  imageUrl:string="";
  posts : any;
  constructor(private httpService: ExternalService) { }

  ngOnInit() {
    this.httpService.getPosts().subscribe({
      next: (posts) => {
        this.posts = posts;
        console.log(this.posts);
      },
      error: (error) => {
       // this.errorMessage = error;
      },
    });

  }
}
