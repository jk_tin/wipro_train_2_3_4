import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormControl, FormGroup, ReactiveFormsModule, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [ReactiveFormsModule,CommonModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {

  loginForm1!: FormGroup;
  // constructor(private loginForm: FormGroup){
  //   this.loginForm1=loginForm;

  // }
  ngOnInit() {
    this.loginForm1 = new FormGroup({

      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', Validators.required),
      captcha:new FormControl('', Validators.required)
    });
  }
  onSubmit() {
    // Handle form submission here
    if (this.loginForm1.valid) {
      console.log(this.loginForm1.value);
      // Additional logic to authenticate user or
      // perform other actions
    }
  }


}
