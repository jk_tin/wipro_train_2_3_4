import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appMyCustomd]',
  standalone: true
})
export class CustomdDirective {


  constructor(private ele: ElementRef) {
  }

  @HostListener('click') onClick() {
    this.ele.nativeElement.style.color = 'red';
  }

  @HostListener('mouseenter') onMousemove() {
    this.ele.nativeElement.style.color ='purple';
  }

  @HostListener('mouseout') onMouseout() {
    this.ele.nativeElement.style.color ='black';
  }

}
