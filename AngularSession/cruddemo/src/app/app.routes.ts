import { Routes } from '@angular/router';
import { IndexComponent } from '../index/index.component';

import { CreateComponent } from '../create/create.component';
import { EditComponent } from '../edit/edit.component';
import { HomeComponent } from '../home/home.component';
import { ViewComponent } from '../view/view.component';

export const routes: Routes = [

  { path: '', redirectTo: 'index', pathMatch: 'full' }, //default route
  { path: 'home', component: HomeComponent },
  { path: 'index', component: IndexComponent },
  { path: 'post', redirectTo: 'post/index', pathMatch: 'full'},
      { path: 'post/index', component: IndexComponent },
      { path: 'post/:postId/view', component: ViewComponent },
      { path: 'post/create', component: CreateComponent },
      { path: 'post/:postId/edit', component: EditComponent }

];
