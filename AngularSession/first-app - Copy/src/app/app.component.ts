import { Component } from '@angular/core';
import { DataComponentComponent } from '../data-component/data-component.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [DataComponentComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'first-app';
  name="Jayanta";

}
