import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {ImgpPipe} from './imgp.pipe'
import { LifecycleComponent } from '../lifecycle/lifecycle.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,ImgpPipe,LifecycleComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'custopipe';
  imageUrl:string="https://m.media-amazon.com/images/M/MV5BYTc1MDQ3NjAtOWEzMi00YzE1LWI2OWUtNjQ0OWJkMzI3MDhmXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_QL75_UX140_CR0,0,140,207_.jpg";
  mycls:string="mycls";

}
