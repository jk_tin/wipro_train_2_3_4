import { AfterViewChecked, AfterViewInit, Component, Input, OnChanges, OnDestroy, OnInit } from '@angular/core';

@Component({
  selector: 'app-lifecycle',
  standalone: true,
  imports: [],
  templateUrl: './lifecycle.component.html',
  styleUrl: './lifecycle.component.css'
})
export class LifecycleComponent   implements OnInit, OnChanges, OnDestroy, AfterViewInit, AfterViewChecked  {

   // Input property
   @Input() data: any;

   constructor() {
    console.log("--constructor--")

    }

   ngOnChanges(changes: any) {
     // Called whenever an input property of the component changes.
     console.log("--ngOnChanges--");
   }

   ngOnInit() {
     // Called once after the component is initialized, and after the first ngOnChanges().
     console.log("--ngOnInit--");

   }

   ngDoCheck() {
     // Called immediately after ngOnChanges() on every change detection run, and immediately after ngOnInit() on the first run.
   }

   ngAfterContentInit() {
     // Called once after the first ngDoCheck().
     // Respond after Angular projects external content into the component's view, or into the view that a directive is in.
   }

   ngAfterContentChecked() {
     // Called after ngAfterContentInit() and every subsequent ngDoCheck().
   }

   ngAfterViewInit() {
     // Called once after the component's view has been initialized and after the first ngAfterContentChecked().
   }

   ngAfterViewChecked() {
     // Called after the ngAfterViewInit() and every subsequent ngAfterContentChecked().
   }

   ngOnDestroy() {
     // Called just before Angular destroys the directive or component.
   }

}
