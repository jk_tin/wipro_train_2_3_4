import { Component, DoCheck } from '@angular/core';

@Component({
  selector: 'app-child2',
  standalone: true,
  imports: [],
  templateUrl: './child2.component.html',
  styleUrl: './child2.component.css'
})
export class Child2Component implements DoCheck {
  ngDoCheck(): void {
    console.log("--Child 2- Change detected---");
  }
}
