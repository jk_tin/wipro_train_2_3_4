import { Component, DoCheck, Input, input } from '@angular/core';
import {Child1Component} from '../child1/child1.component';
import {Child2Component} from '../child2/child2.component';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [Child1Component , Child2Component,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements DoCheck {
  title = 'chngdetectndemo';
  count=1;
  myStr:any[]=[];

  msgFromParent:any[]=[];


  onClick()
  {
      this.count++;
      this.myStr.push("New Change");
      this.msgFromParent.push("Message for Child");

  }

  ngDoCheck(): void {
      console.log("--Change detected---");
  }
}
