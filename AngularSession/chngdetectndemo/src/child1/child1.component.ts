import { CommonModule } from '@angular/common';
import { ChangeDetectionStrategy, Component, DoCheck, Input } from '@angular/core';

@Component({
  selector: 'app-child1',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './child1.component.html',
  styleUrl: './child1.component.css',
  changeDetection:ChangeDetectionStrategy.OnPush
})
export class Child1Component implements DoCheck{

   @Input()
   myMsg:any[]=[];


  ngDoCheck(): void {
    console.log("--Child 1- Change detected---");
  }

  onClick()
  {

    this.myMsg.push("Child1- Message");
  }

}
