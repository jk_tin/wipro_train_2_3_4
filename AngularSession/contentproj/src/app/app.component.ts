import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { SingleSlotComponent } from '../single-slot/single-slot.component';
import { MultiSlotComponent } from '../multi-slot/multi-slot.component';
import { ConditionalContentComponent } from '../conditional-content/conditional-content.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,SingleSlotComponent,MultiSlotComponent,ConditionalContentComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'contentproj';
}
