import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';

import { RegformComponent } from './regform/regform.component';

bootstrapApplication(RegformComponent, appConfig)
  .catch((err) => console.error(err));
