import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormsModule, NgForm } from '@angular/forms';
import { contact } from './contact';

@Component({
  selector: 'app-regform',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './regform.component.html',
  styleUrl: './regform.component.css'
})
export class RegformComponent {
  @ViewChild('contactForm') f!: NgForm;





  ngOnInit() {



  }

  onSubmit() {
    console.log(this.f.value);
  }

}



