import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterOutlet } from '@angular/router';
import { country } from './country';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,FormsModule,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'ngformdemo';

  countryList:country[] = [
    new country("1", "India"),
    new country('2', 'UK'),
    new country('3', 'USA'),
    new country('4', 'GERMANY'),
  ];
  @ViewChild('singUpForm') form: any;

  onSubmit(singUpForm:any) {
    // if (this.form.valid) {
    //   console.log("Form Submitted!");
    //   this.form.reset();
    // }

    console.log(this.form);
    let firstName:string="";


    firstName =singUpForm.value.firstname;
    console.log(singUpForm.value);
  }
}



