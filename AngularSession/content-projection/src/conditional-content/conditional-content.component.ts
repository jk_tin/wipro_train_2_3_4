import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';

@Component({
  selector: 'app-conditional-content',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './conditional-content.component.html',
  styleUrl: './conditional-content.component.css'
})
export class ConditionalContentComponent {
  showContent = false;
}
