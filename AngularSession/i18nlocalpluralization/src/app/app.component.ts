import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'i18nlocalpluralization';
  messages: string[] = ['Message 1', 'Message 2'];
  messageMapping = {
    '=0': 'No messages.',
    '=1': 'One message.',
    'other': '# messages.'
  };
}
