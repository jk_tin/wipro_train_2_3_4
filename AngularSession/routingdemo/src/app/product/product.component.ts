import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-product',
  standalone: true,
  imports: [],
  templateUrl: './product.component.html',
  styleUrl: './product.component.css'
})
export class ProductComponent implements OnInit  {
  productDetail? : any;

  constructor(private route : ActivatedRoute,){}

  ngOnInit(): void {
    let productId = this.route.snapshot.params['name'];
    console.log("productId="+productId);
  }

  // getProductDetailById(id: number){
  //   this.productService.getProductDetailById(id).subscribe(res => {
  //     this.productDetail = res
  //     console.log(res)
  //   })
  // }
}
