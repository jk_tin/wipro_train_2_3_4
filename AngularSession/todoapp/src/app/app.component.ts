import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FirstserviceService } from '../service/firstservice.service';
import { CommonModule } from '@angular/common';
import { FormControl, FormsModule } from '@angular/forms';
import {Topic} from '../interface/Topic'
import {User} from '../interface/User'
@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,CommonModule,FormsModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'todoapp';
   data:any[]=[];
   id:string="";
   name:string="";
   email:string="";
   number:string="";


  constructor(private fisrService:FirstserviceService){

  }

  saveData()
  {

    console.log(this.name);

     user: User={
      id:this.name,
      name: "Juliet Oma",
      email: "julie@yahoo.com",
      number: "08100000000"
    }
    this.fisrService
    .postUser(this.id,this.name,this.email,this.number).subscribe({
      next: (posts:any) => {

        console.log(posts);
      },
      error: (error:any) => {
        console.log(error);
      },
    });

  }

  ngOnInit()
  {
    console.log("--ngOnInit--");
    this.fisrService
    .getPostData().subscribe({
      next: (posts:any) => {
        this.data=posts;
        console.log(posts);
      },
      error: (error:any) => {
        console.log(error);
      },
    });



  }

}
