import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Topic} from '../interface/Topic';
@Injectable({
  providedIn: 'root'
})


export class FirstserviceService {

  private nameList:string[]=['Jayanta','Amit','Vasu'];

  constructor(private httpClient:HttpClient) { }

  getData():string[]
  {
    return this.nameList;

  }



  getPostData():any
  {
    let url ="https://my-json-server.typicode.com/JSGund/XHR-Fetch-Request-JavaScript/posts";


     return this.httpClient.get<Topic[]>(url);

  }


  postUser(id:string,name:string,email:string,number:string):any{
    console.log("--In the post USer Method");
    let userUrl="http://localhost:8000/users";
     return  this.httpClient.post(userUrl, {id:6,name:'Amit',email:'amit@yahoo.com',number:"787797897"});
  }


}
