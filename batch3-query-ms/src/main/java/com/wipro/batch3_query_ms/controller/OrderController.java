package com.wipro.batch3_query_ms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

 
import com.wipro.batch3_query_ms.config.handler.QueryHandler;
import com.wipro.batch3_query_ms.entity.Order;

@RestController
public class OrderController {
	@Autowired
	QueryHandler queryHandler;
	
	@GetMapping("/order")
   public List<Order> findAll(){
		
		return queryHandler.findAll();
	}
	@GetMapping("/order/{id}")
    public Order findById(@PathVariable int id){
		
		return queryHandler.findById(id);
	}

}
