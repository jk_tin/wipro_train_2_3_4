package com.wipro.batch3_query_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch3QueryMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch3QueryMsApplication.class, args);
	}

}
