package com.wipro.batch3_query_ms.config.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wipro.batch3_query_ms.entity.Order;
import com.wipro.batch3_query_ms.repo.OrderRepo;
@Component
public class QueryHandler {
	@Autowired
	OrderRepo orderRepo;
	
//	@KafkaListener(topics = "order-topic", groupId = "batch3-group-id",containerFactory = "kafkaListenerContainerFactory"  ,properties = {"spring.json.value.default.type=com.wipro.batch3_query_ms.entity.Order"})
//	public void handler(Order order)
//	{
//		
//		System.out.println(order);
//		Order orderData=orderRepo.findByOrderId(order.getOrderId());
//		if(null==orderData)
//		{
//			orderRepo.save(orderData);
//		}
//		else
//		{
//			order.setId(orderData.getId());
//			orderRepo.save(orderData);
//		}
//		
//	}
	@KafkaListener(topics = "order-topic", groupId = "batch3-group-id",containerFactory = "kafkaListenerContainerFactory"  ,properties = {"spring.json.value.default.type=com.wipro.batch3_query_ms.entity.Order"})
    public void listen(Order order) {
        System.out.println("Received message: " + order);
        
		Order orderData=orderRepo.findByOrderId(order.getOrderId());
		if(null==orderData)
		{
			orderRepo.save(order);
		}
		else
		{
			order.setId(orderData.getId());
			orderRepo.save(order);
		}
        
    }
	
	public List<Order> findAll()
	{
		return orderRepo.findAll();
		
	}
	public Order findById(int id)
	{
		return orderRepo.findById(id).get();
		
	}
}
