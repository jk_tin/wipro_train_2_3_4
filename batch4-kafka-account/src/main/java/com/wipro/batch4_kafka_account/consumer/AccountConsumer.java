package com.wipro.batch4_kafka_account.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_account.entity.service.AccountService;
import com.wipro.batch4_kafka_account.producer.AccountProducer;
import com.wipro.batch4_kafka_account.vo.AccountVo;
import com.wipro.batch4_kafka_account.vo.*;
 
@Component
public class AccountConsumer {
	@Autowired	
	AccountService accountService;
	
	@Autowired	
	AccountProducer accountProducer;
	
	@KafkaListener(topics = "gpay-acc-1", groupId = "batch4-group-id",
			containerFactory = "kafkaListenerContainerFactory",properties = {"spring.json.value.default.type=com.wipro.batch4_kafka_account.vo.AccountVo"})
	void readFromTopic(AccountVo message)
	{
		String responseMessage=accountService.withdrawMoney(message.getAccountNumber(), message.getAmount());
		ResponseVo responseVO = new ResponseVo();
		responseVO.setAccountNumber(message.getAccountNumber());
		responseVO.setResponseMessage(responseMessage);
		accountProducer.publish("acc-gpay", responseVO);
		
		
	}
}
