package com.wipro.batch4_kafka_account.entity.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.batch4_kafka_account.entity.Account;
import com.wipro.batch4_kafka_account.entity.service.AccountService;
import com.wipro.batch4_kafka_account.repo.AccountRepo;

import jakarta.transaction.Transactional;
@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	AccountRepo accountRepo;
	 
	@Override
	public String withdrawMoney(String accountNumber,double amount) {
		
		Account account=accountRepo.findByAccountNumber(accountNumber);
		if(account.getBalance()>=amount)
		{
			account.setBalance(account.getBalance()-amount);
			accountRepo.save(account);
			return "withdrawal is successful";
		}
		else
		{
			return "insufficient account balance";
		}
		
	 
	}

}
