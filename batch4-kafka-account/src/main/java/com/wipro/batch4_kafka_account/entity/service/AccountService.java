package com.wipro.batch4_kafka_account.entity.service;

public interface AccountService {

	public String withdrawMoney(String accountNumber,double amount);
	
}
