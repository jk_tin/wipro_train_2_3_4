package com.wipro.batch4_kafka_account.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_account.vo.ResponseVo;

@Component
public class AccountProducer {

	@Autowired
	KafkaTemplate kafkaTemplate;
	
	
	
	public void publish(String topicName,  ResponseVo accountReponse)
	{
		kafkaTemplate.send(topicName,accountReponse);
	}
	
	
}
