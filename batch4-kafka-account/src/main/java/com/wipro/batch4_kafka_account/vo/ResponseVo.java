package com.wipro.batch4_kafka_account.vo;

public class ResponseVo {

	String responseMessage;
	String accountNumber;
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	@Override
	public String toString() {
		return "ResponseVo [responseMessage=" + responseMessage + ", accountNumber=" + accountNumber + "]";
	}
	
	
	
}
