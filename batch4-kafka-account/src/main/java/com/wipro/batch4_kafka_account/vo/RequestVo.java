package com.wipro.batch4_kafka_account.vo;

public class RequestVo {
	String accountNumber;
	double amount;
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "AccountVo [accountNumber=" + accountNumber + ", amount=" + amount + "]";
	}
}
