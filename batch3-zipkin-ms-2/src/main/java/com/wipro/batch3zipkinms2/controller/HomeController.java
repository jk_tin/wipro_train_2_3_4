package com.wipro.batch3zipkinms2.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.*;
import com.wipro.batch3zipkinms2.dto.Bore;
import com.wipro.batch3zipkinms2.integration.BoaredService;

 

@RestController
public class HomeController {
	Logger logger= LoggerFactory.getLogger(HomeController.class);
	@Autowired
	RestTemplate restTemplate;
	@Autowired
	BoaredService boaredService;
	
	
	@GetMapping("/hellov1")
	String hello()
	{
		logger.info("--MS2-Hello--");
		String url="https://www.boredapi.com/api/activity/";
		Bore bore=restTemplate.getForObject(url, Bore.class);
		
		return bore.toString();
	}
	@GetMapping("/hello")
	String hellov2()
	{
		logger.info("--MS2-Hello-V2-");
		ResponseEntity<Bore>response=boaredService.getActivity();
		
		return response.getBody().toString();
	}
	

}
