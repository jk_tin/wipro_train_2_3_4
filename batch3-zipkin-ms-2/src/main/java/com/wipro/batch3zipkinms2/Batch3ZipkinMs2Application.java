package com.wipro.batch3zipkinms2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.wipro.batch3zipkinms2.*"})
public class Batch3ZipkinMs2Application {

	public static void main(String[] args) {
		SpringApplication.run(Batch3ZipkinMs2Application.class, args);
	}

}
