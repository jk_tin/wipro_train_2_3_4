package com.wipro.batch3zipkinms2.integration;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.http.*;
import com.wipro.batch3zipkinms2.dto.*;
@FeignClient(name = "BoaredService",
url = "https://www.boredapi.com/api")
public interface BoaredService {
	
	@GetMapping("/activity/")
	ResponseEntity<Bore> getActivity();

}
