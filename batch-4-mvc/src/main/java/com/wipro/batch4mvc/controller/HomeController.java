package com.wipro.batch4mvc.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.wipro.batch4mvc.dto.*;
@Controller
public class HomeController {
	
	@GetMapping("/")
	String showHome()
	{
		return "index";
	}
	
	@GetMapping("/register")
	String register(Model model)
	{
		
		
		User user = new User();		
     	model.addAttribute("user", user);     
        List<String> listProfession = Arrays.asList("Developer", "Tester", "Architect");
        model.addAttribute("listProfession", listProfession);      
        return "register";
	}
	 
	@PostMapping("/register_done")
	String registerDone(@ModelAttribute("user") User user)
	{
		System.out.println(user);
		return "register_success";
	}

}
