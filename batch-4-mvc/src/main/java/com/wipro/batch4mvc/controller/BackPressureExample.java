package com.wipro.batch4mvc.controller;

import reactor.core.publisher.Flux;

public class BackPressureExample {

  public static void main(String[] args) {
    // Simulate a data source emitting a continuous stream of data at a high rate
    Flux<Integer> dataSource = Flux.range(1, Integer.MAX_VALUE);
  
    // Process the data with back pressure using limitRate operator
    dataSource.limitRate(100) // Control the number of elements emitted per request (back pressure)
              .doOnNext(data -> { // Simulate processing delay
                                  try {
                                  Thread.sleep(1000);
                                  }
                                  catch (InterruptedException e) {
                                  e.printStackTrace();
                                  }
                                  System.out.println("Processed: " + data);
                                }
                        ).subscribe();
  }
}