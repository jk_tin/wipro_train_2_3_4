package com.wipro.batch4mvc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4MvcApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4MvcApplication.class, args);
	}

}
