package com.wipro.batch3zipkinms1.controller;

import org.apache.commons.logging.LogFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.wipro.batch3zipkinms1.Batch3ZipkinMs1Application;

@RestController
public class HomeColntroller {
	Logger logger= LoggerFactory.getLogger(HomeColntroller.class);
	@Autowired
	RestTemplate restTemplate;
	
	@GetMapping("/greet")
	String greet()
	{
		logger.info("---Greet--");
		String url= "http://localhost:9002/hello";
		String output=restTemplate.getForObject(url,String.class);
		
		return output;
	}

}
