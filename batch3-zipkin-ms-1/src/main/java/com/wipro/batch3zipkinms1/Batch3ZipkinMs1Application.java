package com.wipro.batch3zipkinms1;

import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@SpringBootApplication
public class Batch3ZipkinMs1Application {

		
	public static void main(String[] args) {
		SpringApplication.run(Batch3ZipkinMs1Application.class, args);
	}

}
