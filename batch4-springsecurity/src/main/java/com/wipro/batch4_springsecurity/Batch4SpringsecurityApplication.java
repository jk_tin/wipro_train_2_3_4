package com.wipro.batch4_springsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4SpringsecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4SpringsecurityApplication.class, args);
	}

}
