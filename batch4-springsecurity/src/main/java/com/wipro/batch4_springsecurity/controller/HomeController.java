package com.wipro.batch4_springsecurity.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
public class HomeController {
	@GetMapping("/hello")
	String sayHello()
	{
		return "Hello World!!!";
	}

}
