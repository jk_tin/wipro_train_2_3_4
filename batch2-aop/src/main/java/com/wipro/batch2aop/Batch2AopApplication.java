package com.wipro.batch2aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class Batch2AopApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch2AopApplication.class, args);
	}

}
