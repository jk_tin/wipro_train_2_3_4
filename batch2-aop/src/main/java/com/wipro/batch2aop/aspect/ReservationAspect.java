package com.wipro.batch2aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ReservationAspect {
	
	@Before("execution(* com.wipro.batch2aop.controller.ReservationController..checkIn(..))")
	void showVacCard()
	{
		System.out.println("--Show Vaccination Card");
	}
	
//	@Before("execution(* com.wipro.batch2aop.controller.ReservationController..checkOut(..))")
//	void sayThanks()
//	{
//		System.out.println("--Thanks for visiting our property-- ");
//	}
	
	 @Around("execution(* com.wipro.batch2aop.controller.ReservationController..checkOut(..))")
	  public  void doBasicProfiling(ProceedingJoinPoint pjp) throws Throwable {
	    System.out.println("--Before Checkout processs starts--");
	    pjp.proceed();
	    System.out.println("--After Checkout processs ends--");
	 
	  }


}
