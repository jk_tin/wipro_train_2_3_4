package com.wipro.batch2aop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {

	@GetMapping("/checkin")
	String checkIn()
	{
		System.out.println("--Checking In---");
		return "Checked In";
		
	}
	@GetMapping("/checkout")
	String checkOut()
	{
		System.out.println("--Checking Out---");
		return "Checked Out";
		
	}
	
	
}
