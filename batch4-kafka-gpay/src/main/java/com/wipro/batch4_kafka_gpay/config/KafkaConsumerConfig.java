package com.wipro.batch4_kafka_gpay.config;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

 
 
import com.wipro.batch4_kafka_gpay.vo.ResponseVo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class KafkaConsumerConfig {

	 @Bean
	    public ConsumerFactory<String, ResponseVo> consumerFactory() {
	        Map<String, Object> configProps = new HashMap<>();
	        configProps.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
	        configProps.put(ConsumerConfig.GROUP_ID_CONFIG, "batch4-group-id");
	        configProps.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
	        configProps.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
	        configProps.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
	        configProps.put(JsonDeserializer.USE_TYPE_INFO_HEADERS,"false");
	        return new DefaultKafkaConsumerFactory<>(configProps);
	    }

	 @Bean
	    public ConcurrentKafkaListenerContainerFactory<String, ResponseVo> kafkaListenerContainerFactory() {
	        ConcurrentKafkaListenerContainerFactory<String, ResponseVo> factory = new ConcurrentKafkaListenerContainerFactory<>();
	        factory.setConsumerFactory(consumerFactory());
	        return factory;
	    }
}