package com.wipro.batch4_kafka_gpay.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4_kafka_gpay.producer.GpayProducer;
import com.wipro.batch4_kafka_gpay.vo.RequestVo;

@RestController
public class GpayController {
	
	@Autowired
	GpayProducer gpayProducer;
	@PostMapping("/pay")
	void requestForMoney(@RequestBody RequestVo request )
	{
		
		gpayProducer.publish("gpay-acc-1", request);
	}

}
