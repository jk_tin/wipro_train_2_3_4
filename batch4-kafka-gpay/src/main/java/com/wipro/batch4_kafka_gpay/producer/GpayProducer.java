package com.wipro.batch4_kafka_gpay.producer;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_gpay.repo.GpayRepo;
import com.wipro.batch4_kafka_gpay.vo.RequestVo;
import com.wipro.batch4_kafka_gpay.entity.*;
@Component
public class GpayProducer {
	@Autowired
	KafkaTemplate kafkaTemplate;
	
	@Autowired
	GpayRepo gpayRepo;
	
 	public void publish(String topicName,RequestVo request)
	{
 		
 		GpayEntity gpayEntity= new GpayEntity();
 		UUID uuid = UUID.randomUUID();
 		String uuidAsString = uuid.toString();
 		gpayEntity.setTransdactionNumber(uuidAsString);
 		gpayEntity.setAmount(request.getAmount()); 		
 		gpayEntity.setAccountNumber(request.getAccountNumber());
 		gpayRepo.save(gpayEntity);	
 		
		kafkaTemplate.send(topicName,request);
		
	}
	

}
