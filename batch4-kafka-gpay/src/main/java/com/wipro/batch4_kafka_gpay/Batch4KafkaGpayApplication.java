package com.wipro.batch4_kafka_gpay;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4KafkaGpayApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4KafkaGpayApplication.class, args);
	}

}
