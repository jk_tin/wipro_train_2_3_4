package com.wipro.batch4_kafka_gpay.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_gpay.vo.ResponseVo;

 
@Component
public class GpayConsumer {
	
	@KafkaListener(topics = "acc-gpay", groupId = "batch4-group-id"
			,properties = {"spring.json.value.default.type=com.wipro.batch4_kafka_gpay.vo.ResponseVo"})
	void readFromTopic(ResponseVo message)
	{
	   System.out.println(message);
		
	}

}
