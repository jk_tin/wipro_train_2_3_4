package com.wipro.batch4_kafka_gpay.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.batch4_kafka_gpay.entity.GpayEntity;

@Repository
public interface GpayRepo extends JpaRepository<GpayEntity, Integer> {

}
