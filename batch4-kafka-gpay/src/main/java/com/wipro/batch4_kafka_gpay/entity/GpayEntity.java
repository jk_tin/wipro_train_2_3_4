package com.wipro.batch4_kafka_gpay.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="gpay_account")
public class GpayEntity {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	@Column(name="transaction_number")
	String transdactionNumber;
	
	@Column(name="account_number")
	String accountNumber;

	@Column(name="amount")	
	double amount;
	
	@Column(name="payment_status")
	String paymentStatus;
	public String getPaymentStatus() {
		return paymentStatus;
	}
	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTransdactionNumber() {
		return transdactionNumber;
	}
	public void setTransdactionNumber(String transdactionNumber) {
		this.transdactionNumber = transdactionNumber;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	@Override
	public String toString() {
		return "GpayEntity [id=" + id + ", transdactionNumber=" + transdactionNumber + ", accountNumber="
				+ accountNumber + ", amount=" + amount + ", paymentStatus=" + paymentStatus + "]";
	}
	
	
	
	
	
	
	
	
	
}
