package com.wipro.springdocker41.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/greet")
	String greet()
	{
		
		return "Hello From Docker";
	}
	
	
}
