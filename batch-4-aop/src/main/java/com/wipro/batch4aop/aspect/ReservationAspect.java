package com.wipro.batch4aop.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ReservationAspect {
	 
	@Before("execution(* com.wipro.batch4aop.controller.ReservationController..checkIn(..))")
	void showVacCard()
	{
		System.out.println(" - Checking Vaccination card-");
		
	}
	@After("execution(* com.wipro.batch4aop.controller.ReservationController..checkIn(..))")
	void showVacCardOver()
	{
		System.out.println(" - Thank you for showing your Vaccination card-");
		
	}
	
	@Before("execution(* com.wipro.batch4aop.controller.ReservationController..checkOut(..))")
	void giveGift()
	{
		System.out.println(" - Thank You for visting us-");
		
	}

}
