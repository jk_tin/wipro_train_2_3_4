package com.wipro.batch4aop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
@EnableAspectJAutoProxy(proxyTargetClass=true)
public class Batch4AopApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4AopApplication.class, args);
	}

}
