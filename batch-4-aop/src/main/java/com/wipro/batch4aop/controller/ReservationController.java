package com.wipro.batch4aop.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReservationController {
	
	@GetMapping("/checkin")
	String checkIn()
	{
		System.out.println("--Chcekin In--");
		return "Checked In";
		
	}
	@GetMapping("/checkout")
	String checkOut()
	{
		System.out.println("--Chceking Out--");
		return "Checking Out";
		
	}

}
