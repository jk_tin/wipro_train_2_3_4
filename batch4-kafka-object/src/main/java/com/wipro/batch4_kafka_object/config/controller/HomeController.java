package com.wipro.batch4_kafka_object.config.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.wipro.batch4_kafka_object.producer.MessageProducer;
import com.wipro.batch4_kafka_object.vo.News;
@RestController
public class HomeController {
	
	@Autowired
	MessageProducer messageProducer;
	
	@PostMapping("/news")
	void postMessage(@RequestBody News news)
	{
		messageProducer.publishMessage("latest-news-1", news);
		
		
	}

}
