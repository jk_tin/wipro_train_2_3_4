package com.wipro.batch4_kafka_object.vo;

public class News {
	
	String category;
	String newsDesc;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getNewsDesc() {
		return newsDesc;
	}
	public void setNewsDesc(String newsDesc) {
		this.newsDesc = newsDesc;
	}
	@Override
	public String toString() {
		return "News [category=" + category + ", newsDesc=" + newsDesc + "]";
	}
	
	

}
