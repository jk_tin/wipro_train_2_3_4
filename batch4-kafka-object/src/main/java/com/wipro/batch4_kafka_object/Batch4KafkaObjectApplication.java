package com.wipro.batch4_kafka_object;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Batch4KafkaObjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(Batch4KafkaObjectApplication.class, args);
	}

}
