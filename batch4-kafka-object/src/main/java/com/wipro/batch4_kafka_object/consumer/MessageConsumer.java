package com.wipro.batch4_kafka_object.consumer;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_object.vo.News;

@Component
public class MessageConsumer {
	@KafkaListener(topics = "latest-news-1", groupId = "batch4-group-id"
			,properties = {"spring.json.value.default.type=com.wipro.batch4_kafka_object.vo.News"})
	void readFromTopic(News message)
	{
		
		System.out.println(message);
	}
}
