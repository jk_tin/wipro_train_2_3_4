package com.wipro.batch4_kafka_object.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.wipro.batch4_kafka_object.vo.News;

@Component
public class MessageProducer {

	@Autowired
	KafkaTemplate kafkaTemplate;

	public void publishMessage(String topicName, News news)
	{
		kafkaTemplate.send(topicName,news);
		
	}
	
	
}
