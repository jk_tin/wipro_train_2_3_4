package com.wipro.react1.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

	@GetMapping("/hello")
	String sayHello()
	{
		return "Hello From Docker";
		
	}
	
}
